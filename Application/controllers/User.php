<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller
{

    function __construct()
    {
        parent:: __construct();
        $this->load->model('user_handling', 'user');
    }

    function index()
    {
        $this->load->view('layout/header.php');
        $this->load->view('layout/sidebar.php');
        $this->load->view('user/listUser');
        $this->load->view('layout/footer.php');
    }

    public function ajax_list()
    {
        $list = $this->user->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $user) {
            $no++;
            $row = array();
            $row[] = $user->id;
            $row[] = $user->first_name;
            $row[] = $user->last_name;
            $row[] = $user->username;
            $row[] = $user->password;
            $row[] = $user->email;
            $row[] = $user->created_date;
            $row[] = $user->group_id;


            $row[] = '<a class="btn btn-sm btn-primary openBtn1" id="edit" onclick="editForm(' . "'" . $user->id . "'" . ')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>
				  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_user(' . "'" . $user->id . "'" . ')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->user->count_all(),
            "recordsFiltered" => $this->user->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }

    public function addForm()
    {
        $this->load->view("user/addForm");
    }

    public function ajax_add()
    {
        $this->validate_add();
        $data = array(
            'first_name' => $this->input->post('firstName'),
            'last_name' => $this->input->post('lastName'),
            'username' => $this->input->post('userName'),
            'password' => $this->input->post('passw'),
            'cfmpassword' => $this->input->post('cfmpass'),
            'email' => $this->input->post('email'),
            'created_date' => $this->input->post('createDate'),
            'group_id' => $this->input->post('groupId'),
        );
        $insert = $this->user->save($data);
        echo json_encode(array("status" => TRUE));
    }

    public function ajax_edit($id)
    {
        $data = $this->user->get_by_id($id);
        $data = array(
            'id' => $data->id,
            'firstName' => $data->first_name,
            'lastName' => $data->last_name,
            'userName' => $data->username,
            'passw' => $data->password,
            'cfmpass' => $data->cfmpassword,
            'email' => $data->email,
            'createDate' => $data->created_date,
            'groupId' => $data->group_id,
        );
        $this->load->view("user/editForm", $data);
    }

    public function ajax_update()
    {
        $this->validate_add();
        $data = array(
            'first_name' => $this->input->post('firstName'),
            'last_name' => $this->input->post('lastName'),
            'username' => $this->input->post('userName'),
            'password' => $this->input->post('passw'),
            'cfmpassword' => $this->input->post('cfmpass'),
            'email' => $this->input->post('email'),
            'created_date' => $this->input->post('createDate'),
            'group_id' => $this->input->post('groupId'),
        );
        $this->user->update(array('id' => $this->input->post('id')), $data);
        echo json_encode(array("status" => TRUE));
    }

    public function ajax_delete($id)
    {
        $this->user->delete_by_id($id);
        echo json_encode(array("status" => TRUE));
    }

    public function validate_add()
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;

        if ($this->input->post('firstName') == '') {
            $data['inputerror'][] = 'firstName';
            $data['error_string'][] = 'First name is required';
            $data['status'] = FALSE;
        }

        if ($this->input->post('lastName') == '') {
            $data['inputerror'][] = 'lastName';
            $data['error_string'][] = 'Last name is required';
            $data['status'] = FALSE;
        }

        if ($this->input->post('userName') == '') {
            $data['inputerror'][] = 'userName';
            $data['error_string'][] = 'Username is required';
            $data['status'] = FALSE;
        }
        if ($this->input->post('passw') == '') {
            $data['inputerror'][] = 'passw';
            $data['error_string'][] = 'Psasword is required';
            $data['status'] = FALSE;
        }
        if ($this->input->post('cfmpass') == '') {
            $data['inputerror'][] = 'cfmpass';
            $data['error_string'][] = 'Please confirm password';
            $data['status'] = FALSE;
        }
        if ($this->input->post('email') == '') {
            $data['inputerror'][] = 'email';
            $data['error_string'][] = 'Email is required';
            $data['status'] = FALSE;
        }
        if ($this->input->post('createDate') == '') {
            $data['inputerror'][] = 'createDate';
            $data['error_string'][] = 'Date is required';
            $data['status'] = FALSE;
        }
        echo json_encode($data);
    }

    public function validate_edit()
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;

        if ($this->input->post('firstName') == '') {
            $data['inputerror'][] = 'firstName';
            $data['error_string'][] = 'First name is required';
            $data['status'] = FALSE;
        }

        if ($this->input->post('lastName') == '') {
            $data['inputerror'][] = 'lastName';
            $data['error_string'][] = 'Last name is required';
            $data['status'] = FALSE;
        }

        if ($this->input->post('userName') == '') {
            $data['inputerror'][] = 'userName';
            $data['error_string'][] = 'Username is required';
            $data['status'] = FALSE;
        }
        if ($this->input->post('passw') == '') {
            $data['inputerror'][] = 'passw';
            $data['error_string'][] = 'Psasword is required';
            $data['status'] = FALSE;
        }
        if ($this->input->post('cfmpass') == '') {
            $data['inputerror'][] = 'cfmpass';
            $data['error_string'][] = 'Please confirm password';
            $data['status'] = FALSE;
        }
        if ($this->input->post('email') == '') {
            $data['inputerror'][] = 'email';
            $data['error_string'][] = 'Email is required';
            $data['status'] = FALSE;
        }
        if ($this->input->post('createDate') == '') {
            $data['inputerror'][] = 'createDate';
            $data['error_string'][] = 'Date is required';
            $data['status'] = FALSE;
        }
        echo json_encode($data);
    }

}
