<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CategoryController extends CI_Controller
{

    function __construct()
    {
        parent:: __construct();
        $this->load->model('Category', 'm');
        $this->load->model('Category');

        $this->load->library('form_validation');
        $this->load->library('session');
    }

    function index()
    {
        $data['categorys'] = $this->m->getCategory();
        $this->load->view('layout/header');
        $this->load->view('layout/sidebar');
        $this->load->view('category/index', $data);
        $this->load->view('layout/footer');
    }

    public function add()
    {
        $this->load->view('layout/header');
        $this->load->view('layout/sidebar');
        $this->load->view('category/add');
        $this->load->view('layout/footer');
    }

    function validation_add()
    {
        $this->form_validation->set_rules('categoryName', 'Category Name', 'required');
    }

    public function submit()
    {
        if ($this->input->post('btnSave') == 'Save') {
            $field = array(
                'inventory_category_name' => $this->input->post('categoryName'),
            );
            $this->validation_add();
            if ($this->form_validation->run() == FALSE) {
                $this->add();
            } else {
                $this->Category->insertCategory($field);
               /* $this->db->insert('inventory_category', $field);*/
                $this->session->set_flashdata('item', 'Added successfully');
                redirect(base_url('category'));
            }
        } else {
            redirect('error/show');
        }
    }

    public function edit($id)
    {
        $category = $this->m->getCategoryById($id);
        if ($category == NULL) {
            redirect('error/show');
        } else {
            $data['category'] = $this->m->getCategoryById($id);
            $this->showForm($data);
        }
    }

    public function showForm($data)
    {
        $this->load->view('layout/header');
        $this->load->view('layout/sidebar');
        $this->load->view('category/edit', $data);
        $this->load->view('layout/footer');
    }

    public function update()
    {
        if ($this->input->post('btnUpdate') == 'Update') {
            $id = $this->input->post('txt_hidden');
            $field = array(
                'inventory_category_name' => $this->input->post('categoryName'),
            );
            $this->validation_edit();
            if ($this->form_validation->run() == FALSE) {
                $category = new stdClass();
                $category->inventory_category_id = $this->input->post('txt_hidden');
                $category->inventory_category_name = $this->input->post('categoryName');
                $data = array();
                $data['category'] = $category;
                $this->showForm($data);
            } else {
                $this->Category->updateCategory($field, $id);
                $this->session->set_flashdata('item', 'Updated successfully');
                redirect(base_url('category'));
            }
        } else {
            redirect('error/show');
        }
    }

    function validation_edit()
    {
        $this->form_validation->set_rules('categoryName', 'Category Name', 'required');
    }

    public function delete($id)
    {
        $this->Category->deleteCategory($id);
        redirect(base_url('category'));
    }

}