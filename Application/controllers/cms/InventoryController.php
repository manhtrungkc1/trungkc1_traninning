<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class InventoryController extends CI_Controller
{

    function __construct()
    {
        parent:: __construct();
        $this->load->model('Inventory', 'm');
        $this->load->model('Inventory');
        $this->load->model('Category', 'inventoryCategory');

        $this->load->library('form_validation');
        $this->load->library('session');
    }

    function index()
    {
        $data['inventorys'] = $this->m->getInventory();
        $this->load->view('layout/header');
        $this->load->view('layout/sidebar');
        $this->load->view('inventory/index', $data);
        $this->load->view('layout/footer');
    }

    public function add()
    {
        $data['inventory_category'] = $this->inventoryCategory->getCategory();
        $this->load->view('layout/header');
        $this->load->view('layout/sidebar');
        $this->load->view('inventory/add', $data);
        $this->load->view('layout/footer');
    }

    function validation_add()
    {
        $this->form_validation->set_rules('inventoryName', 'Inventory Name', 'required');
        $this->form_validation->set_rules('inventoryCategory', 'Inventory Category', 'required');
        $this->form_validation->set_rules('rentType', 'Rent type', 'required');
    }

    public function submit()
    {
        if ($this->input->post('btnSave') == 'Save') {
            $field = array(
                'inventory_name' => $this->input->post('inventoryName'),
                'rent_type' => $this->input->post('rentType'),
                'inventory_category_id' => $this->input->post('inventoryCategory'),
            );
            $this->validation_add();
            if ($this->form_validation->run() == FALSE) {
                $this->add();
            } else {
                $this->Inventory->insertInventory($field);
                redirect(base_url('inventory'));
            }
        } else {
            redirect('error/show');
        }
    }

    public function edit($id)
    {
        $inventory = $this->m->getInventoryById($id);
        if ($inventory == NULL) {
            redirect('error/show');
        } else {
            $data['inventory'] = $this->m->getInventoryById($id);
            $data['inventory_category'] = $this->inventoryCategory->getCategory();
            $this->showForm($data);
        }
    }

    public function showForm($data)
    {
        $this->load->view('layout/header');
        $this->load->view('layout/sidebar');
        $this->load->view('inventory/edit', $data);
        $this->load->view('layout/footer');
    }

    public function update()
    {
        if ($this->input->post('btnUpdate') == 'Update') {
            $id = $this->input->post('txt_hidden');
            $field = array(
                'inventory_name' => $this->input->post('inventoryName'),
                'rent_type' => $this->input->post('rentType'),
                'inventory_category_id' => $this->input->post('inventoryCategory'),
            );
            $this->validation_edit();
            if ($this->form_validation->run() == FALSE) {
                $inventory = new stdClass();
                $inventory->inventory_id = $this->input->post('txt_hidden');
                $inventory->inventory_name = $this->input->post('inventoryName');
                $inventory->rent_type = $this->input->post('rentType');
                $inventory->inventory_category_id = $this->input->post('inventoryCategory');
                $data = array();
                $data['inventory'] = $inventory;
                $this->showForm($data);
            } else {
                $this->Inventory->updateInventory($field, $id);
                $this->session->set_flashdata('item', 'Updated successfully');
                redirect(base_url('inventory'));
            }
        } else {
            redirect('error/show');
        }
    }

    function validation_edit()
    {
        $this->form_validation->set_rules('inventoryName', 'Inventory Name', 'required');
        $this->form_validation->set_rules('rentType', 'Rent type', 'required');
        $this->form_validation->set_rules('inventoryCategory', 'Inventory Category', 'required');
    }

    public function delete($id)
    {
        $this->Inventory->deleteInventory($id);
        redirect(base_url('inventory'));
    }

}