<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class InventoryRentalController extends CI_Controller
{
    function __construct()
    {
        parent:: __construct();
        $this->load->model('InventoryRental', 'm');
        $this->load->model('InventoryRental');
        $this->load->model('Inventory', 'inventoryName');
        $this->load->model('InventoryRentalDetail', 'inventoryDetail');
        $this->load->model('InventoryRentalDetail');

        $this->load->library('form_validation');
        $this->load->library('session');
    }

    function index()
    {
        $data['inventoryOrders'] = $this->m->getInventoryRental();
        $this->load->view('layout/header');
        $this->load->view('layout/sidebar');
        $this->load->view('inventoryrental/index', $data);
        $this->load->view('layout/footer');
    }

    public function add()
    {
        $data['order_id'] = $this->m->getIncrementOrderId();
        $data['inventory_name'] = $this->inventoryName->getInventory();
        $order_prefix = $this->config->item('order_prefix');
        $data['order_prefix'] = $order_prefix;
        $this->load->view('layout/header');
        $this->load->view('layout/sidebar');
        $this->load->view('inventoryrental/add', $data);
        $this->load->view('layout/footer');
    }

    public function submit()
    {
        $row = array();
        $row2 = array();

        if (isset($_POST['order_id'])) {
            $order_date = $this->input->post('order_date');
            date_default_timezone_set("Asia/Bangkok");
            $order_date = DateTime::createFromFormat('d-m-Y H:i:s', $order_date)->format('Y-m-d H:i:s');
            $field = array(
                'order_id' => $this->input->post('order_id'),
                'company' => $this->input->post('company'),
                'representative' => $this->input->post('representative'),
                'location' => $this->input->post('location'),
                'country' => $this->input->post('country'),
                'state' => $this->input->post('state'),
                'order_date' => $order_date,
                'sales_person' => $this->input->post('sales_person'),
                'delivered_by' => $this->input->post('delivered_by'),
                'afe' => $this->input->post('afe'),
                'truck' => $this->input->post('truck'),
                'mileage' => $this->input->post('mileage'),
                'trailer' => $this->input->post('trailer'),
                'order_type' => $this->input->post('order_type'),
                'order_status' => $this->input->post('order_status'),
            );

            if (isset($_POST['inventory_id'])) {
                $order_id = $this->input->post('order_id');

                foreach ($_POST['inventory_id'] as $key) {

                    if (isset($_POST['del_pu'][$key])) {
                        $del_pu = 1;
                    } else {
                        $del_pu = 0;
                    }

                    $inventoryDetail = array(
                        'order_id' => $this->input->post('order_id'),
                        'inventory_id' => $_POST['inventory_id'][$key],
                        'inventory_name' => $_POST['inventory_name'][$key],
                        'quantity' => $_POST['quantity'][$key],
                        'unitnumber' => $_POST['unitnumber'][$key],
                        'start' => $_POST['start'][$key],
                        'finish' => $_POST['finish'][$key],
                        'delpu' => $del_pu,
                        'of_days' => $_POST['ofdays'][$key],
                        'priceferday' => $_POST['priceferday'][$key],
                        'delivery_fee' => $_POST['delivery_fee'][$key],
                        'comment' => $_POST['comment'][$key],
                        'days_miles' => $_POST['days_miles'][$key]
                    );

                    $inventoryDetail2 = array(
                        'order_id' => $this->input->post('order_id'),
                        'inventory_id' => $_POST['inventory_id'][$key],
                        'quantity' => $_POST['quantity'][$key],
                        'unitnumber' => $_POST['unitnumber'][$key],
                        'start' => $_POST['start'][$key],
                        'finish' => $_POST['finish'][$key],
                        'delpu' => $del_pu,
                        'of_days' => $_POST['ofdays'][$key],
                        'delivery_fee' => $_POST['delivery_fee'][$key],
                        'comment' => $_POST['comment'][$key],
                        'days_miles' => $_POST['days_miles'][$key]
                    );

                    $row[] = $inventoryDetail;
                    $row2[] = $inventoryDetail2;
                }

            }
        }
        $this->validation_add();
        if($this->form_validation->run() == FALSE){

            $this->session->set_userdata('table', $row);
            $this->add();

        }
        else{
            $this->InventoryRentalDetail->deleteInventoryRentalDetail($order_id);
            $this->InventoryRental->insertRentalOrder($field);
            foreach ($row2 as $value){
                $this->InventoryRentalDetail->insertInventoryRentalDetail($value);
            }

            redirect('inventoryrental');
        }
    }

function validation_add()
{
    $this->form_validation->set_rules('company', 'Company', 'required|min_length[4]');
    $this->form_validation->set_rules('representative', 'Company Representative', 'required|min_length[4]');
    $this->form_validation->set_rules('location', 'Location', 'required|min_length[4]');
    $this->form_validation->set_rules('country', 'Country', 'required|min_length[4]');
    $this->form_validation->set_rules('state', ' State', 'required|min_length[4]');
    $this->form_validation->set_rules('order_date', 'Order Date', 'required');
    $this->form_validation->set_rules('sales_person', 'Sale Person', 'required');
    $this->form_validation->set_rules('delivered_by', 'Delivered by', 'required');
    $this->form_validation->set_rules('afe', 'APE # ', 'required');
    $this->form_validation->set_rules('truck', 'Truck #', 'required');
    $this->form_validation->set_rules('mileage', 'Mileage', 'required|is_numeric');
    $this->form_validation->set_rules('trailer', 'Trailer', 'required');
    $this->form_validation->set_rules('order_type', 'Order Type', 'required');
    $this->form_validation->set_rules('order_status', 'Order Status', 'required');
}

public
function edit($id)
{
    $order_prefix = $this->config->item('order_prefix');
    $data['order_prefix'] = $order_prefix;
    $inventory = $this->m->getInventoryOrderById($id);
    if ($inventory == NULL) {
        redirect('error/show');
    } else {
        $data['inventory_name'] = $this->inventoryName->getInventory();
        $data['inventory_order'] = $this->m->getInventoryOrderById($id);
        $data['inventory_detail'] = $this->inventoryDetail->getInventoryDetail($id);
        $this->showForm($data);
    }
}

public
function editRetail($id)
{
    $data['inventory_name'] = $this->inventoryName->getInventory();
    $data['inventory_order'] = $this->m->getInventoryOrderById($id);
    $data['inventory_detail'] = $this->inventoryDetail->getInventoryDetail($id);
    $this->showFormEditRetail($data);
}

public
function showFormEditRetail($data)
{
    $this->load->view('inventoryrental/editretail', $data);
}

public
function delete()
{
    $order_id_detail = $this->input->post('order_id_detail');
    $inventory_id_detail = $this->input->post('inventory_id_detail');
    if ($order_id_detail) {
        $this->db->where('order_id', $order_id_detail);
        $this->db->where('inventory_id', $inventory_id_detail);
        $this->db->delete('inventory_rental_order_detail');
    }
}

public
function showForm($data)
{
    $this->load->view('layout/header');
    $this->load->view('layout/sidebar');
    $this->load->view('inventoryrental/edit', $data);
    $this->load->view('layout/footer');
}

public
function update()
{
    if ($this->input->post('btnUpdate') == 'Update') {
        $id = $this->input->post('order_id');
        $field = array(
            'company' => $this->input->post('company'),
            'representative' => $this->input->post('representative'),
            'location' => $this->input->post('location'),
            'country' => $this->input->post('country'),
            'state' => $this->input->post('state'),
            'order_date' => $this->input->post('order_date'),
            'sales_person' => $this->input->post('sales_person'),
            'delivered_by' => $this->input->post('delivered_by'),
            'afe' => $this->input->post('afe'),
            'truck' => $this->input->post('truck'),
            'mileage' => $this->input->post('mileage'),
            'trailer' => $this->input->post('trailer'),
            'order_type' => $this->input->post('order_type'),
            'order_status' => $this->input->post('order_status')
        );

        if (isset($_POST['inventory_id'])) {
            $order_id = $this->input->post('order_id');
            $this->InventoryRentalDetail->deleteInventoryRentalDetail($order_id);
            foreach ($_POST['inventory_id'] as $key) {

                if (isset($_POST['del_pu'][$key])) {
                    $del_pu = 1;
                } else {
                    $del_pu = 0;
                }

                $inventoryDetail = array(
                    'order_id' => $this->input->post('order_id'),
                    'inventory_id' => $_POST['inventory_id'][$key],
                    'quantity' => $_POST['quantity'][$key],
                    'unitnumber' => $_POST['unitnumber'][$key],
                    'start' => $_POST['start'][$key],
                    'finish' => $_POST['finish'][$key],
                    'delpu' => $del_pu,
                    'of_days' => $_POST['ofdays'][$key],
                    'delivery_fee' => $_POST['delivery_fee'][$key],
                    'comment' => $_POST['comment'][$key],
                    'days_miles' => $_POST['days_miles'][$key]
                );
                $this->InventoryRentalDetail->insertInventoryRentalDetail($inventoryDetail);
            }
        }

        $this->InventoryRental->updateRentalOrder($field, $id);

        $this->session->set_flashdata('item', 'Updated successfully');
        redirect(base_url('inventoryrental'));
    } else {
        redirect('error/show');
    }
}

function validation_edit()
{
    $this->form_validation->set_rules('company', 'Company', 'required');
    $this->form_validation->set_rules('representative', 'Company Representative', 'required');
    $this->form_validation->set_rules('location', 'Location', 'required');
    $this->form_validation->set_rules('country', 'Country', 'required');
    $this->form_validation->set_rules('state', 'State', 'required');
    $this->form_validation->set_rules('order_date', 'Order Date', 'required');
    $this->form_validation->set_rules('sales_person', 'Sale Person', 'required');
    $this->form_validation->set_rules('delivered_by', 'Delivered by', 'required');
    $this->form_validation->set_rules('afe', 'APE # ', 'required');
    $this->form_validation->set_rules('truck', 'Truck #', 'required');
    $this->form_validation->set_rules('mileage', 'Mileage', 'required');
    $this->form_validation->set_rules('trailer', 'Trailer', 'required');
    $this->form_validation->set_rules('order_type', 'Order Type', 'required');
    $this->form_validation->set_rules('order_status', 'Order Status', 'required');
}
}