<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserGroupController extends CI_Controller
{

    function __construct()
    {
        parent:: __construct();
        $this->load->model('UserGroup', 'm');
        $this->load->model('UserGroup');

        $this->load->library('form_validation');
        $this->load->library('session');
    }

    function index()
    {
        $data['usergroups'] = $this->m->getUserGroup();
        $this->load->view('layout/header');
        $this->load->view('layout/sidebar');
        $this->load->view('usergroup/index', $data);
        $this->load->view('layout/footer');
    }

    public function add()
    {
        $this->load->view('layout/header');
        $this->load->view('layout/sidebar');
        $this->load->view('usergroup/add');
        $this->load->view('layout/footer');
    }

    function validation_add()
    {
        $this->form_validation->set_rules('groupName', 'Group Name', 'required');
    }

    public function submit()
    {
        if ($this->input->post('btnSave') == 'Save') {
            $field = array(
                'group_name' => $this->input->post('groupName'),
                'management_group' => $this->input->post('management_group'),
                'delete_inventory' => $this->input->post('delete_inventory'),
                'edit_inventory' => $this->input->post('edit_inventory'),
            );
            $this->validation_add();
            if ($this->form_validation->run() == FALSE) {
                $this->add();
            } else {
                $this->UserGroup->insertGroup($field);
                $this->session->set_flashdata('item', 'Added Group Successfully');
                redirect(base_url('usergroup'));
            }
        } else {
            redirect('error/show');
        }
    }

    public function edit($id)
    {
        $usergroup = $this->m->getUserGroupById($id);
        if ($usergroup == NULL) {
            redirect('error/show');
        } else {
            $data['group'] = $this->m->getUserGroupById($id);
            $this->showFormEdit($data);
        }
    }

    public function showFormEdit($data)
    {
        $this->load->view('layout/header');
        $this->load->view('layout/sidebar');
        $this->load->view('usergroup/edit', $data);
        $this->load->view('layout/footer');
    }

    public function update()
    {
        if ($this->input->post('btnUpdate') == 'Update') {
            $id = $this->input->post('txt_hidden');
            $field = array(
                'group_name' => $this->input->post('group_name'),
                'management_group' => $this->input->post('management_group'),
                'delete_inventory' => $this->input->post('delete_inventory'),
                'edit_inventory' => $this->input->post('edit_inventory'),
            );
            $this->validation_edit();
            if ($this->form_validation->run() == FALSE) {
                $usergroup = new stdClass();
                $usergroup->group_id = $this->input->post('txt_hidden');
                $usergroup->group_name = $this->input->post('group_name');
                $usergroup->management_group = $this->input->post('management_group');
                $usergroup->delete_inventory = $this->input->post('delete_inventory');
                $usergroup->edit_inventory = $this->input->post('edit_inventory');
                $data = array();
                $data['group'] = $usergroup;
                $this->showFormEdit($data);
            } else {
                $this->UserGroup->updateGroup($field,$id);
                $this->session->set_flashdata('item', 'Updated successfully');
                redirect(base_url('usergroup'));
            }
        } else {
            redirect('error/show');
        }
    }

    function validation_edit()
    {
        $this->form_validation->set_rules('group_name', 'Group Name', 'required');
    }

    public function delete($id)
    {
        $this->UserGroup->deleteGroup($id);
        redirect(base_url('usergroup'));
    }

}