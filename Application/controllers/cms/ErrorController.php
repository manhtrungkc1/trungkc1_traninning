<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ErrorController extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
    }

    public function error()
    {
        $this->load->view('layout/header');
        $this->load->view('layout/sidebar');
        $this->load->view('user/error-page');
        $this->load->view('layout/footer');
    }
}