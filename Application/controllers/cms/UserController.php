<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class UserController extends CI_Controller
{

    function __construct()
    {
        parent:: __construct();
        $this->load->model('User');
        $this->load->model('User','m');
        $this->load->model('UserGroup', 'userGroup');

        $this->load->library('form_validation');
        $this->load->library('session');

        $this->load->helper('url');
    }

    function index()
    {
        $this->load->library('pagination');
        $perpage = 10;
        $config['base_url'] = base_url('user/');
        $config['per_page'] = $perpage;
        $config['total_rows'] = $this->User->count_all('user');
        $config['full_tag_open'] = '<li class="pagination_button">';
        $config['full_tag_close'] = '</li>';
        $config['cur_tag_open']= '<li class="pagination_button active"><a href="">';
        $config['cur_tag_close']= '</a></li>';
        $config['num_tag_open'] = '<li class="pagination_button">';
        $config['num_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li class="pagination_button">';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li class="pagination_button">';
        $config['next_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $pagination = $this->pagination->create_links();
        $data['pagination'] = $pagination;
        $start = $this->uri->segment(2);
        $data['users'] = $this->User->listUser2($config['per_page'],$start);
        $this->load->view('layout/header');
        $this->load->view('layout/sidebar');
        $this->load->view('user/index', $data);
        $this->load->view('layout/footer');
    }

    public function submit()
    {
        if ($this->input->post('btnSave') == 'Save') {
            $field = array(
                'username' => $this->input->post('userName'),
                'password' => $this->input->post('passw'),
                'email' => $this->input->post('email'),
                'group_id' => $this->input->post('groupId'),
                'phone' => $this->input->post('phone'),
                'created_at' => $this->input->post('createDate'),
            );
            $this->validation_add();
            if ($this->form_validation->run() == FALSE) {
                $this->add();
            } else {
                $this->User->insert($field);
               /* $this->db->insert('user', $field);*/
                $this->session->set_flashdata('item', 'Added successfully');
                redirect(base_url('user'));
            }
        } else {
            redirect('error/show');
        }
    }

    function validation_add()
    {
        $this->form_validation->set_rules('userName', 'Username', 'required');
        $this->form_validation->set_rules('passw', 'Password', 'required');
        $this->form_validation->set_rules('cfmpass', 'Confirm Password', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('groupId', 'User Group', 'required');
        $this->form_validation->set_rules('phone', 'Phone', 'required');
        $this->form_validation->set_rules('createDate', 'Create Date', 'required');
    }

    public function add()
    {
        $data['user_group'] = $this->userGroup->getUserGroup();
        $this->load->view('layout/header');
        $this->load->view('layout/sidebar');
        $this->load->view('user/add', $data);
        $this->load->view('layout/footer');
    }

    public function edit($id)
    {
        $user = $this->m->getUserById($id);
        if ($user == NULL) {
            redirect('error/show');
        } else {
            $data['user'] = $this->m->getUserById($id);
            $data['user_group'] = $this->userGroup->getUserGroup();
            $this->showForm($data);
        }
    }

    public function showForm($data)
    {
        $this->load->view('layout/header');
        $this->load->view('layout/sidebar');
        $this->load->view('user/edit', $data);
        $this->load->view('layout/footer');
    }

    public function update()
    {
        if ($this->input->post('btnUpdate') == 'Update') {
            $id = $this->input->post('txt_hidden');
            $field = array(
                'username' => $this->input->post('userName'),
                'password' => $this->input->post('passw'),
                'email' => $this->input->post('email'),
                'group_id' => $this->input->post('groupId'),
                'phone' => $this->input->post('phone'),
            );
            $this->validation_edit();
            if ($this->form_validation->run() == FALSE) {
                $user = new stdClass();
                $user->user_id = $this->input->post('txt_hidden');
                $user->username = $this->input->post('userName');
                $user->password = $this->input->post('passw');
                $user->password = $this->input->post('cfmpass');
                $user->email = $this->input->post('email');
                $user->group_id = $this->input->post('groupId');
                $user->phone = $this->input->post('phone');
                $user->created_at = $this->input->post('date');
                $data = array();
                $data['user'] = $user;
                $this->showForm($data);
            } else {
                $this->User->updateUser($field,$id); //truyen mang field, bien id
                $this->session->set_flashdata('item', 'Updated successfully');
                redirect(base_url('user'));
            }
        } else {
            redirect('error/show');
        }
    }

    function validation_edit()
    {
        $this->form_validation->set_rules('userName', 'Username', 'required');
        $this->form_validation->set_rules('passw', 'Password', 'required');
        $this->form_validation->set_rules('cfmpass', 'Confirm Password', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('phone', 'Phone', 'required');
    }

    public function delete($id)
    {
        $this->User->deleteUser($id);
        redirect(base_url('user'));
    }

}