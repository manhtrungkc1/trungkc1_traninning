<div class="content-wrapper">
    <section class="content-header">
        <?php if ($this->session->flashdata('item')) { ?>
            <div class="alert alert-success">
                <?php echo $this->session->flashdata('item'); ?>
            </div>
        <?php } ?>
    </section>
    <section class="content container-fluid">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">List User Group</h3>
                <br/>
                <a href="<?php echo base_url('usergroup/add'); ?>" class="btn btn-primary"><i
                            class="glyphicon glyphicon-plus"></i> Add User Group</a>
            </div>
            <div class="box-body">
                <table class="table table-striped table-hover table-bordered">
                    <thead>
                    <tr>
                        <th>Group ID</th>
                        <th>Group Name</th>
                        <th>Management_group</th>
                        <th>Delete Inventory</th>
                        <th>Edit Inventory</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if ($usergroups) {
                        foreach ($usergroups as $usergroup) {
                            ?>
                            <tr>
                                <td><?php echo $usergroup->group_id; ?></td>
                                <td><?php echo $usergroup->group_name; ?></td>
                                <td><?php echo $usergroup->management_group; ?></td>
                                <td><?php echo $usergroup->delete_inventory; ?></td>
                                <td><?php echo $usergroup->edit_inventory; ?></td>
                                <td>
                                    <a href="<?php echo base_url('usergroup/edit/' . $usergroup->group_id); ?>"
                                       class="btn btn-info">Edit</a>
                                    <a href="<?php echo base_url('usergroup/delete/' . $usergroup->group_id); ?>"
                                       class="btn btn-danger"
                                       onclick="return confirm('Do you want to delete this inventory?');">Delete</a>
                                </td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>
<?php
