<div class="content-wrapper">
    <section class="content container-fluid">
        <section class="content-header">
            <h3>Edit Group</h3>
            <a href="<?php echo base_url('usergroup'); ?>" class="btn btn-default">Back</a>
        </section>

        <?php if (validation_errors()) { ?>
            <div class="alert alert-error">
                <?php echo validation_errors(); ?>
            </div>
        <?php } ?>

        <div class="box-body">
            <form action="<?php echo base_url('usergroup/saveEdit') ?>" method="post" class="form-horizontal">
                <input type="hidden" value="<?php echo $group->group_id; ?>" name="txt_hidden"/>
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Group Name: </label>
                        <div class="col-md-5">
                            <input name="group_name" placeholder="" value="<?php echo $group->group_name ?>"
                                   class="form-control" type="text">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Management Group:</label>
                        <div class="col-md-1 form-check-inline">
                            <label class="form-check-label">
                                <?php
                                if ($group->management_group == 1) {
                                    ?>
                                    <label><input class="form-check-input" checked="checked" type="radio"
                                                  name="management_group" value="1">Yes</label>
                                    <label><input class="form-check-input" type="radio" name="management_group"
                                                  value="0">No</label>
                                    <?php
                                } else {
                                    ?>
                                    <label><input class="form-check-input" type="radio" name="management_group"
                                                  value="1">Yes</label>
                                    <label><input class="form-check-input" checked="checked" type="radio"
                                                  name="management_group" value="0">No</label>
                                    <?php
                                }
                                ?>
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Delete Inventory:</label>
                        <div class="col-md-1 form-check-inline">
                            <label class="form-check-label">
                                <?php
                                if ($group->delete_inventory == 1) {
                                    ?>
                                    <label><input class="form-check-input" checked="checked" type="radio"
                                                  name="delete_inventory" value="1">Yes</label>
                                    <label><input class="form-check-input" type="radio" name="delete_inventory"
                                                  value="0">No</label>
                                    <?php
                                } else {
                                    ?>
                                    <label><input class="form-check-input" type="radio" name="delete_inventory"
                                                  value="1">Yes</label>
                                    <label><input class="form-check-input" checked="checked" type="radio"
                                                  name="delete_inventory" value="0">No</label>
                                    <?php
                                }
                                ?>
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Edit Inventory:</label>
                        <div class="col-md-1 form-check-inline">
                            <label class="form-check-label">
                                <?php
                                if ($group->edit_inventory == 1) {
                                    ?>
                                    <label><input class="form-check-input" checked="checked" type="radio"
                                                  name="edit_inventory" value="1">Yes</label>
                                    <label><input class="form-check-input" type="radio" name="edit_inventory" value="0">No</label>
                                    <?php
                                } else {
                                    ?>
                                    <label><input class="form-check-input" type="radio" name="edit_inventory" value="1">Yes</label>
                                    <label><input class="form-check-input" checked="checked" type="radio"
                                                  name="edit_inventory" value="0">No</label>
                                    <?php
                                }
                                ?>
                            </label>
                        </div>
                    </div>
                    <div class="modal-footer col-md-8">
                        <input type="submit" name="btnUpdate" class="btn btn-primary" value="Update">
                        <a href="<?php echo base_url('usergroup'); ?>" class="btn btn-default">Cancel</a>
                    </div>
                </div>
            </form>
        </div>
    </section>
</div>

<?php
