<div class="content-wrapper">
    <section class="content container-fluid">
        <section class="content-header">
            <h3>Create Group</h3>
            <a href="<?php echo base_url('usergroup'); ?>" class="btn btn-default">Back</a>
        </section>

        <?php if (validation_errors()) { ?>
            <div class="alert alert-error">
                <?php echo validation_errors(); ?>
            </div>
        <?php } ?>

        <div class="box-body">
            <form action="<?php echo base_url('usergroup/saveNew') ?>" method="post" class="form-horizontal">
                <input type="hidden" value="" name="id"/>
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Group Name: </label>
                        <div class="col-md-5">
                            <input name="groupName" placeholder="" class="form-control" type="text">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Management Group:</label>
                        <div class="col-md-1 form-check-inline">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="management_group" value="1">Yes
                            </label>
                        </div>
                        <div class="col-md-1 form-check-inline">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="management_group" value="0">No
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Delete Inventory:</label>
                        <div class="col-md-1 form-check-inline">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="delete_inventory" value="1">Yes
                            </label>
                        </div>
                        <div class="col-md-1 form-check-inline">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="delete_inventory" value="0">No
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Edit Inventory:</label>
                        <div class="col-md-1 form-check-inline">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="edit_inventory" value="1">Yes
                            </label>
                        </div>
                        <div class="col-md-1 form-check-inline">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="edit_inventory" value="0">No
                            </label>
                        </div>
                    </div>
                    <div class="modal-footer col-md-8">
                        <input type="submit" name="btnSave" class="btn btn-primary" value="Save">
                        <a href="<?php echo base_url('usergroup'); ?>" class="btn btn-default">Cancel</a>
                    </div>
                </div>
            </form>
        </div>
    </section>
</div>

