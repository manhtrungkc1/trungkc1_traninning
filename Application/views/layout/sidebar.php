<aside class="main-sidebar">
    <section class="sidebar">
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?php echo base_url('assets/') ?>dist/img/user-admin.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>Admin</p>
                <a><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>


        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">HEADER</li>
            <li><a href="<?php echo base_url('user'); ?>"><i class="fa fa-user" aria-hidden="true"></i><span>User</span></a>
            </li>
            <li><a href="<?php echo base_url('usergroup'); ?>"><i class="fa fa-users" aria-hidden="true"></i> <span>User Group</span></a>
            </li>
            <li><a href="<?php echo base_url('category'); ?>"><i class="fa fa-link"></i> <span>Category</span></a></li>
            <li><a href="<?php echo base_url('inventory'); ?>"><i class="fa fa-link"></i> <span>Inventory</span></a>
            </li>
            <li><a href="<?php echo base_url('inventoryrental'); ?>"><i class="fa fa-list-alt"
                                                                        aria-hidden="true"></i><span>Inventory Rental</span></a>
            </li>
            <li><a href="<?php echo base_url('inventoryrental/add'); ?>"><i class="fa fa-pencil-square-o"
                                                                            aria-hidden="true"></i> <span>Inventory Rentail</span></a>
            </li>
        </ul>
    </section>
</aside>
