<div class="content-wrapper">
    <section class="content-header">
        <?php if ($this->session->flashdata('item')) { ?>
            <div class="alert alert-success">
                <?php echo $this->session->flashdata('item'); ?>
            </div>
        <?php } ?>
    </section>
    <section class="content container-fluid">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">List Inventory</h3>
                <br/>
                <a href="<?php echo base_url('inventory/add'); ?>" class="btn btn-primary"><i
                            class="glyphicon glyphicon-plus"></i> Add Inventory</a>
            </div>
            <div class="box-body">
                <table class="table table-striped table-hover table-bordered">
                    <thead>
                    <tr>
                        <th>Inventory ID</th>
                        <th>Inventory Name</th>
                        <th>Rent Type</th>
                        <th>Category</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if ($inventorys) {
                        foreach ($inventorys as $inventory) {
                            ?>
                            <tr>
                                <td><?php echo $inventory->inventory_id; ?></td>
                                <td><?php echo $inventory->inventory_name; ?></td>
                                <td><?php echo $inventory->rent_type; ?></td>
                                <td><?php echo $inventory->inventory_category_name; ?></td>
                                <td>
                                    <a href="<?php echo base_url('inventory/edit/' . $inventory->inventory_id); ?>"
                                       class="btn btn-info">Edit</a>
                                    <a href="<?php echo base_url('inventory/delete/' . $inventory->inventory_id); ?>"
                                       class="btn btn-danger"
                                       onclick="return confirm('Do you want to delete this inventory?');">Delete</a>
                                </td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>
<?php
