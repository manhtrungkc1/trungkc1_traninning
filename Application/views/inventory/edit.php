<div class="content-wrapper">
    <section class="content container-fluid">
        <section class="content-header">
            <h3>Edit Inventory</h3>
            <a href="<?php echo base_url('inventory'); ?>" class="btn btn-default">Back</a>
        </section>

        <?php if (validation_errors()) { ?>
            <div class="alert alert-error">
                <?php echo validation_errors(); ?>
            </div>
        <?php } ?>

        <div class="box-body">
            <form action="<?php echo base_url('inventory/saveEdit') ?>" method="post" class="form-horizontal">
                <input type="hidden" name="txt_hidden" value="<?php echo $inventory->inventory_id; ?>">
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Inventory Name: </label>
                        <div class="col-md-5">
                            <input name="inventoryName" value="<?php echo $inventory->inventory_name; ?>"
                                   class="form-control" type="text">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Rent Type:</label>
                        <div class="form-check-inline col-md-2">
                            <select name="rentType" value="<?php echo $inventory->rent_type; ?>" class="form-control"
                                    id="sel1">
                                <option value="Day">Day</option>
                                <option value="Mile">Mile</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Category: </label>
                        <div class="form-check-inline col-md-2">
                            <select class="form-control" name="inventoryCategory">
                                <?php
                                $inventoryCategory = $inventory->inventory_category_id;
                                ?>
                                <?php foreach ($inventory_category as $category) { ?>
                                    <?php if ($inventoryCategory == $category->inventory_category_id) { ?>
                                        <option type="radio" name="inventoryCategory"
                                                value="<?php echo $category->inventory_category_id ?>"
                                                selected> <?php echo $category->inventory_category_name; ?></option>
                                    <?php } else { ?>
                                        <option type="radio" name="inventoryCategory"
                                                value="<?php echo $category->inventory_category_id ?>"> <?php echo $category->inventory_category_name; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer col-md-8">
                        <input type="submit" name="btnUpdate" class="btn btn-primary" value="Update">
                        <a href="<?php echo base_url('inventory'); ?>" class="btn btn-default">Cancel</a>
                    </div>
                </div>
            </form>
        </div>
    </section>
</div>

