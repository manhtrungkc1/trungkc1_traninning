<div class="content-wrapper">
    <section class="content container-fluid">
        <section class="content-header">
            <h3>Create Inventory</h3>
            <a href="<?php echo base_url('inventory'); ?>" class="btn btn-default">Back</a>
        </section>

        <?php if (validation_errors()) { ?>
            <div class="alert alert-error">
                <?php echo validation_errors(); ?>
            </div>
        <?php } ?>

        <div class="box-body">
            <form action="<?php echo base_url('inventory/saveNew') ?>" method="post" class="form-horizontal">
                <input type="hidden" value="" name="id"/>
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Inventory Name: </label>
                        <div class="col-md-5">
                            <input name="inventoryName" placeholder="" class="form-control" type="text"
                                   value="<?php echo set_value('inventoryName'); ?>">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Category: </label>
                        <div class="form-check-inline col-md-3">
                            <select class="form-control" name="inventoryCategory">
                                <option value="">--Select of Category--</option>
                                <?php foreach ($inventory_category as $invenCategory) { ?>
                                    <option value="<?php echo $invenCategory->inventory_category_id ?>" <?php echo
                                    set_select('inventoryCategory', $invenCategory->inventory_category_id); ?> ><?php echo $invenCategory->inventory_category_name ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Rent Type:</label>
                        <div class="form-check-inline col-md-3">
                            <select name="rentType" class="form-control" id="sel1">
                                <option value="">--Select of Rent type--</option>
                                <option value="Day" <?php echo
                                set_select('rentType', "Day"); ?> >Day
                                </option>
                                <option value="Mile" <?php echo
                                set_select('rentType', "Mile"); ?>>Mile
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer col-md-8">
                        <input type="submit" name="btnSave" class="btn btn-primary" value="Save">
                        <a href="<?php echo base_url('inventory'); ?>" class="btn btn-default">Cancel</a>
                    </div>
                </div>
            </form>
        </div>
    </section>
</div>

