<table id="myTable" class="table table-hover table-bordered table-striped">
    <thead>
    <tr>
        <th></th>
        <th class="row">Equipment</th>
        <th>Quantity</th>
        <th>Unit number</th>
        <th>Start</th>
        <th>Finish</th>
        <th>Del/Pu</th>
        <th># of days</th>
        <th>Price per day</th>
        <th>#miles/FLT Rate</th>
        <th>DeliveryFee Yes/No</th>
        <th>Comments</th>
    </tr>
    </thead>
    <tbody>
    <?php
    if ($inventory_detail > 0) {
        foreach ($inventory_detail as $inventoryDetail) { ?>
            <tr id="<?php echo $inventoryDetail->inventory_id ?>">
                <td><button type="button" onclick="remove(this)" size="6px"
                           data-orderid="<?php echo $inventory_order->order_id; ?>"
                           data-inventoryid="<?php echo $inventoryDetail->inventory_id; ?>" class='fa fa-trash' aria-hidden='true'> </button>
                    <input type="hidden"
                           name="inventory_id[<?php echo $inventoryDetail->inventory_id; ?>]"
                           value="<?php echo $inventoryDetail->inventory_id; ?>"/>
                </td>
                <td><input name="inventory_name[<?php echo $inventoryDetail->inventory_id; ?>]"
                           type="text" size="5px" id="inventory_detail_id"
                           value="<?php echo $inventoryDetail->inventory_name; ?>"/></td>
                <td><input name="quantity[<?php echo $inventoryDetail->inventory_id; ?>]"
                           type="text"
                           size="5px"
                           value="<?php echo $inventoryDetail->quantity ?>"/></td>
                <td><input type="text" size="5px"
                           name="unitnumber[<?php echo $inventoryDetail->inventory_id; ?>]"
                           value="<?php echo $inventoryDetail->unitnumber ?>"/></td>
                <td><input size="5px"
                           name="start[<?php echo $inventoryDetail->inventory_id; ?>]"
                           type="date"
                           value="<?php echo $inventoryDetail->start ?>"/></td>
                <td><input size="5px"
                           name="finish[<?php echo $inventoryDetail->inventory_id; ?>]"
                           type="date"
                           value="<?php echo $inventoryDetail->finish ?>"/></td>
                <td><input type="checkbox"
                           name="del_pu[<?php echo $inventoryDetail->inventory_id; ?>]"
                           value="<?php echo $inventoryDetail->delpu ?>" <?php if ($inventoryDetail->delpu == 1) echo 'checked' ?> /></td>
                <td><input size="3px"
                           name="ofdays[<?php echo $inventoryDetail->inventory_id; ?>]"
                           type="text"
                           value="<?php echo $inventoryDetail->days_miles ?>"/></td>
                <td><input size="3px"
                           name="priceFerDay[<?php echo $inventoryDetail->inventory_id; ?>]"
                           type="text"
                           value="<?php echo $inventoryDetail->days_miles ?>"/></td>
                <td><input size="3px"
                           name="days_miles[<?php echo $inventoryDetail->inventory_id; ?>]"
                           type="text"
                           value="<?php echo $inventoryDetail->days_miles ?>"/></td>
                <td><select class="form-control"
                            name="delivery_fee[<?php echo $inventoryDetail->inventory_id; ?>]">
                        <?php if ($inventoryDetail->delivery_fee == 1) { ?>
                            <option value="1" selected>Yes</option>
                            <option value="0">No</option>
                        <?php } else { ?>
                            <option value="0" selected>No</option>
                            <option value="1">Yes</option>
                        <?php } ?>
                    </select>
                </td>
                <td><input name="comment[<?php echo $inventoryDetail->inventory_id; ?>]"
                           type="text" value="<?php echo $inventoryDetail->comment ?>"/></td>
            </tr>

            <?php
        }
    }
    ?>
    </tbody>
</table>
<div>
    <label style="color: red">Comment</label>
    <textarea class="form-control"></textarea>
</div>