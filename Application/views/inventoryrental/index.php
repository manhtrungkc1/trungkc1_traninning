<div class="content-wrapper">
    <section class="content-header">
        <?php if ($this->session->flashdata('item')) { ?>
            <div class="alert alert-success">
                <?php echo $this->session->flashdata('item'); ?>
            </div>
        <?php } ?>
    </section>
    <section class="content container-fluid">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Inventory Rental</h3>
                <br/>
                <a href="<?php echo base_url('inventoryrental/add'); ?>" class="btn btn-primary"><i
                            class="glyphicon glyphicon-plus"></i> Add Inventory Rental</a>
            </div>

            <div class="box-body">
                <div class="table-responsive">
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>Order ID</th>
                            <th>Company</th>
                            <th>Representative</th>
                            <th>Location</th>
                            <th>Country</th>
                            <th>State</th>
                            <th>Order Type</th>
                            <th>Order Status</th>
                            <th>Sales Person</th>
                            <th>Truck</th>
                            <th>Trailer</th>
                            <th>Order Date</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if ($inventoryOrders) {
                            foreach ($inventoryOrders as $inventoryOrder) {
                                ?>
                                <tr>
                                    <td><?php echo $inventoryOrder->order_id; ?></td>
                                    <td><?php echo $inventoryOrder->company; ?></td>
                                    <td><?php echo $inventoryOrder->representative; ?></td>
                                    <td><?php echo $inventoryOrder->location; ?></td>
                                    <td><?php echo $inventoryOrder->country; ?></td>
                                    <td><?php echo $inventoryOrder->state; ?></td>
                                    <td><?php echo $inventoryOrder->order_type; ?></td>
                                    <td><?php echo $inventoryOrder->order_status; ?></td>
                                    <td><?php echo $inventoryOrder->sales_person; ?></td>
                                    <td><?php echo $inventoryOrder->truck; ?></td>
                                    <td><?php echo $inventoryOrder->trailer; ?></td>
                                    <td><?php echo $inventoryOrder->order_date; ?></td>
                                    <td class="text-nowrap">
                                        <a href="<?php echo base_url('inventoryrental/edit/' . $inventoryOrder->order_id); ?>"
                                           class="btn btn-info">Edit</a>
                                        <a href="<?php echo base_url('inventoryrental/delete/' . $inventoryOrder->order_id); ?>"
                                           class="btn btn-danger"
                                           onclick="return confirm('Do you want to delete this inventory?');">Delete</a>
                                    </td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
</div>
<?php
