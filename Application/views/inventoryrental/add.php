<div class="content-wrapper">
    <section class="content container-fluid">
        <div class="box">
            <form action="<?php echo base_url('inventoryrental/saveNew'); ?>" method="post" class="form-horizontal" id="addOrderForm">
                <center><h1>KC1 Project Job/ Inventory Rental</h1></center>
                <?php if (validation_errors()) { ?>
                    <div class="alert alert-error">
                        <?php echo validation_errors(); ?>
                    </div>
                <?php } ?>
                <div class="container">
                    <div id="addOrderInventoryForm">
                        <div class="col-lg-6">
                            <div class="row">
                                <div class="form-group">
                                    <label class="control col-md-4">Inventory Rental ID: </label>
                                    <span style="color:red;font-weight: bold"><?php echo $order_prefix . "-" . $order_id; ?></span>
                                    <input type="hidden" name="order_id" value="<?php echo $order_id; ?>"/>
                                </div>
                                <div class="form-group">
                                    <label class="control col-md-2">Company: </label>
                                    <div class="col-md-9">
                                        <input name="company" placeholder="" class="form-control" type="text"
                                               value="<?php echo set_value('company'); ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control col-md-4">Company Representative: </label>
                                    <div class="col-md-7">
                                        <input name="representative" placeholder="" class="form-control" type="text"
                                               value="<?php echo set_value('representative'); ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control col-md-2">Location: </label>
                                    <div class="col-md-9">
                                        <input name="location" placeholder="" class="form-control" type="text"
                                               value="<?php echo set_value('location'); ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control col-md-2">Country: </label>
                                    <div class="col-md-3">
                                        <input name="country" placeholder="" class="form-control" type="text"
                                               value="<?php echo set_value('country'); ?>">
                                    </div>
                                    <label class="control col-md-2">State: </label>
                                    <div class="col-md-4">
                                        <input name="state" placeholder="" class="form-control" type="text"
                                               value="<?php echo set_value('state'); ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="row">
                                <div class="form-group">
                                    <label class="control col-md-5">Order Date: </label>
                                    <div class="col-md-5">
                                        <input class="form-control" name="order_date" value="<?php
                                        date_default_timezone_set("Asia/Bangkok");
                                        $d = strtotime("now");
                                        echo date("d-m-Y H:i:s", $d);
                                        ?>"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control col-md-3">Sales Person: </label>
                                    <div class="col-md-9">
                                        <input name="sales_person" placeholder="" class="form-control" type="text"
                                               value="<?php echo set_value('sales_person'); ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control col-md-3">Delivered By: </label>
                                    <div class="col-md-9">
                                        <input name="delivered_by" placeholder="" class="form-control" type="text"
                                               value="<?php echo set_value('delivered_by'); ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control col-md-3">AFE #: </label>
                                    <div class="col-md-3">
                                        <input name="afe" placeholder="" class="form-control" type="text"
                                               value="<?php echo set_value('afe'); ?>">
                                    </div>
                                    <label class="control col-md-2">Truck #: </label>
                                    <div class="col-md-4">
                                        <input name="truck" placeholder="" class="form-control" type="text"
                                               value="<?php echo set_value('truck'); ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control col-md-3">Mileages: </label>
                                    <div class="col-md-3">
                                        <input name="mileage" placeholder="" class="form-control" type="text"
                                               value="<?php echo set_value('mileage'); ?>">
                                    </div>
                                    <label class="control col-md-2">Trailer: </label>
                                    <div class="col-md-4">
                                        <input name="trailer" placeholder="" class="form-control" type="text"
                                               value="<?php echo set_value('trailer'); ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-2">
                                <label class="radio-inline">
                                    <input type="radio" name="order_type" value="Pump Down" checked>Pump Down
                                </label>
                            </div>
                            <div class="col-md-2">
                                <label class="radio-inline">
                                    <input type="radio" name="order_type" value="Frac Prep">Frac Prep
                                </label>
                            </div>
                            <div class="col-md-2">
                                <label class="radio-inline">
                                    <input type="radio" name="order_type" value="Frac">Frac
                                </label>
                            </div>
                            <div class="col-md-2">
                                <label class="radio-inline">
                                    <input type="radio" name="order_type" value="Drill Out">Drill Out
                                </label>
                            </div>
                            <div class="col-md-2">
                                <label class="radio-inline">
                                    <input type="radio" name="order_type" value="Install Tubing">Install Tubing
                                </label>
                            </div>
                            <div class="col-md-2">
                                <label class="radio-inline">
                                    <input type="radio" name="order_type" value="Coil Tubing">Coil Tubing
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-2">
                                <label class="radio-inline">
                                    <input type="radio" name="order_type" value="Flow Back Completions" >Flow Back Completions
                                </label>
                            </div>
                            <div class="col-md-2">
                                <label class="radio-inline">
                                    <input type="radio" name="order_type" value="Flow Back Production">Flow Back Production
                                </label>
                            </div>
                            <div class="col-md-2">
                                <label class="radio-inline">
                                    <input type="radio" name="order_type" value="Hydrostatic Pressure Test">Hydrostatic Pressure Test
                                </label>
                            </div>
                            <div class="col-md-2">
                                <label class="radio-inline">
                                    <input type="radio" name="order_type" value="Misc">Misc
                                </label>
                            </div>
                            <div class="col-md-2">
                                <label class="radio-inline">
                                    <input type="radio" name="order_type" value="Running Production">Running Production
                                </label>
                            </div>
                            <div class="col-md-2">
                                <label class="radio-inline">
                                    <input type="radio" name="order_type" value="Backhoe/Dump Truck">Backhoe/Dump Truck
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-7">
                                <div class="control-label col-md-1">
                                    <label style="color: red">Status:</label>
                                </div>
                                <div class="col-md-2">
                                    <label class="radio-inline">
                                        <input type="radio" name="order_status" checked>Open
                                    </label>
                                </div>
                                <div class="col-md-2">
                                    <label class="radio-inline">
                                        <input type="radio" name="order_status">Closed
                                    </label>
                                </div>
                                <div class="col-md-2">
                                    <label class="radio-inline">
                                        <input type="radio" name="order_status">Approved
                                    </label>
                                </div>
                                <div class="col-md-2">
                                    <label class="radio-inline">
                                        <input type="radio" name="order_status" value="Invoiced">Invoiced
                                    </label>
                                </div>
                                <div class="col-md-2">
                                    <label class="radio-inline">
                                        <input type="radio" name="order_status" value="Assigned">Assigned
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-2">
                            <select class="form-control" id="inventoryName" name="inventoryName">
                                <option value="0">Select Inventory</option>
                                <?php foreach ($inventory_name as $inventoryName) { ?>
                                    <option value="<?php echo $inventoryName->inventory_id ?>" <?php echo
                                    set_select('inventoryCategory', $inventoryName->inventory_name); ?> ><?php echo $inventoryName->inventory_name ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-lg-2">
                            <th colspan="12">
                                <input type="button" class="btn btn-info" onclick="add()" value="Add Inventory">
                            </th>
                        </div>
                    </div>
                    <table id="myTable" class="table table-hover table-bordered table-striped">
                        <thead>
                        <tr>
                            <th></th>
                            <th class="row">Equipment</th>
                            <th>Quantity</th>
                            <th>Unit number</th>
                            <th>Start</th>
                            <th>Finish</th>
                            <th>Del/Pu</th>
                            <th># of days</th>
                            <th>Price per day</th>
                            <th>#miles/FLT Rate</th>
                            <th>DeliveryFee Yes/No</th>
                            <th>Comments</th>
                        </tr>
                        </thead>
                        <tbody id="table1">
                        <?php
                        $infoRetail = $this->session->userdata('table');
                            if (count($infoRetail) > 0) {
                                foreach ($infoRetail as $inventoryRetail) {
                                    ?>
                                    <tr>
                                        <td>
                                            <button type="button" onclick="remove(this)" size="6px"
                                                    data-orderid="<?php echo $inventoryRetail['order_id']; ?>"
                                                    data-inventoryid="<?php echo $inventoryRetail['order_id']; ?>"
                                                    class='fa fa-trash' aria-hidden='true'></button>
                                            <input type="hidden"
                                                   name="inventory_id[<?php echo $inventoryRetail['inventory_id']; ?>]"
                                                   value="<?php echo $inventoryRetail['inventory_id']; ?>"/>
                                        </td>
                                        <td><input name="inventory_name[<?php echo $inventoryRetail['inventory_id']; ?>]"
                                                   type="text" size="5px" id="inventory_detail_id"
                                                   value="<?php echo $inventoryRetail['inventory_name']; ?>"
                                                   readonly="true" /></td>
                                        <td><input name="quantity[<?php echo $inventoryRetail['inventory_id']; ?>]"
                                                   type="text"
                                                   size="5px"
                                                   value="<?php echo $inventoryRetail['quantity']; ?>"/></td>
                                        <td><input type="text" size="5px"
                                                   name="unitnumber[<?php echo $inventoryRetail['inventory_id']; ?>]"
                                                   value="<?php echo $inventoryRetail['unitnumber']; ?>"/></td>
                                        <td><input size="5px"
                                                   name="start[<?php echo $inventoryRetail['inventory_id']; ?>]"
                                                   type="date"
                                                   value="<?php echo $inventoryRetail['start'] ?>"/></td>
                                        <td><input size="5px"
                                                   name="finish[<?php echo $inventoryRetail['inventory_id']; ?>]"
                                                   type="date"
                                                   value="<?php echo $inventoryRetail['finish']; ?>"/></td>
                                        <td><input type="checkbox"
                                                   name="del_pu[<?php echo $inventoryRetail['inventory_id']; ?>]"
                                                   value="<?php echo $inventoryRetail['delpu']; ?>" <?php if ($inventoryRetail['delpu'] == 1) echo 'checked' ?> />
                                        </td>
                                        <td><input size="3px"
                                                   name="ofdays[<?php echo $inventoryRetail['inventory_id']; ?>]"
                                                   type="text"
                                                   value="<?php echo $inventoryRetail['of_days']; ?>"
                                            required /></td>
                                        <td><input size="3px"
                                                   name="priceferday[<?php echo $inventoryRetail['inventory_id']; ?>]"
                                                   type="text"
                                                   value="<?php echo $inventoryRetail['priceferday']; ?>"/></td>
                                        <td><input size="3px"
                                                   name="days_miles[<?php echo $inventoryRetail['inventory_id']; ?>]"
                                                   type="text"
                                                   value="<?php echo $inventoryRetail['days_miles'] ?>"/></td>
                                        <td><select class="form-control"
                                                    name="delivery_fee[<?php echo $inventoryRetail['inventory_id']; ?>]">
                                                <?php if ($inventoryRetail['delivery_fee'] == 1) { ?>
                                                    <option value="1" selected>Yes</option>
                                                    <option value="0">No</option>
                                                <?php } else { ?>
                                                    <option value="0" selected>No</option>
                                                    <option value="1">Yes</option>
                                                <?php } ?>
                                            </select>
                                        </td>
                                        <td><input name="comment[<?php echo $inventoryRetail['inventory_id']; ?>]"
                                                   type="text" value="<?php echo $inventoryRetail['comment']; ?>"/></td>
                                    </tr>
                                    <?php
                                }
                                $this->session->unset_userdata('table');
                            }
                        ?>
                        </tbody>
                        <tfood>
                            <tr>
                                <th colspan="12">
                                    <label style="color: red">Comment</label>
                                    <textarea class="form-control"></textarea>
                                </th>
                            </tr>
                        </tfood>
                    </table>
                    <div class="form-group modal-footer">
                        <input type="button" class="btn btn-danger" value="Print">
                        <button type="button" class="btn btn-primary">Reset</button>
                        <input type="submit" name="btnSave" class="btn btn-info" value="Save">
                        <button type="button" class="btn btn-light">Cancel</button>
                    </div>
                </div>
            </form>
        </div>
    </section>
</div>
<script>

    function add() {
        var tBody = $("#myTable > TBODY")[0];
        var length = $('#inventoryName > option').length;
        var varSelect = $('#inventoryName').val();
        if (varSelect == 0) {
        } else {
            if (length > 0) {
                row = tBody.insertRow(-1);
                var cell = $(row.insertCell(-1));
                var btnRemove = $("<button />");
                btnRemove.attr("type", "button");
                btnRemove.append("<i class='fa fa-trash' aria-hidden='true'></i>");
                btnRemove.attr("onclick", "remove(this);");
                btnRemove.attr("size", "6px");
                var idHidden = $("<input />");
                var idInven = $('#inventoryName').val();
                idHidden.attr("value", idInven);
                idHidden.attr("name", "inventory_id[" + idInven + "]");
                idHidden.attr("type", "hidden");
                cell.append(btnRemove, idHidden);
                var cell = $(row.insertCell(-1));
                var inventoryName = $("<input />");
                var idInventory = $('#inventoryName').val();
                var nameInventory = $('#inventoryName :selected').text();
                inventoryName.attr("type", "text");
                inventoryName.prop('required',true);
                inventoryName.prop('readonly',true);
                inventoryName.attr("id", "inventory_detail_id");
                inventoryName.attr("name", "inventory_name" + "[" + idInventory + "]");
                inventoryName.attr("value", nameInventory);
                inventoryName.attr("size", "5px");
                $("select[name=inventoryName] option:selected").remove();
                cell.append(inventoryName);
                var cell = $(row.insertCell(-1));
                var quantity = $("<input />");
                quantity.attr({
                    type: 'text',
                    required: true,
                    name:   'quantity' + '[' + idInventory + ']',
                    size:   '5px'
                });
                cell.html(quantity);
                var cell = $(row.insertCell(-1));
                var unitnumber = $("<input />");
                unitnumber.attr("type", "text");
                unitnumber.attr("value", "");
                unitnumber.attr("name", "unitnumber" + "[" + idInventory + "]");
                unitnumber.attr("size", "5px");
                unitnumber.attr("required", "true");
                cell.html(unitnumber);
                var cell = $(row.insertCell(-1));
                var start = $("<input />");
                start.attr("type", "date");
                start.attr("name", "start" + "[" + idInventory + "]");
                start.attr("size", "5px");
                start.attr("required", "true");
                cell.html(start);
                var cell = $(row.insertCell(-1));
                var finish = $("<input />");
                finish.attr("type", "date");
                finish.attr("name", "finish" + "[" + idInventory + "]");
                finish.attr("size", "5px");
                finish.attr("required", "true");
                cell.html(finish);
                var cell = $(row.insertCell(-1));
                var delpu = $("<input />");
                delpu.attr("type", "checkbox");
                delpu.attr("name", "del_pu" + "[" + idInventory + "]");
                delpu.attr("size", "5px");
                cell.html(delpu);
                var cell = $(row.insertCell(-1));
                var ofdays = $("<input />");
                ofdays.attr("type", "text");
                ofdays.attr("name", "ofdays" + "[" + idInventory + "]");
                ofdays.attr("size", "3px");
                ofdays.attr("required", "true");
                cell.html(ofdays);
                var cell = $(row.insertCell(-1));
                var priceFerDay = $("<input />");
                priceFerDay.attr("type", "text");
                priceFerDay.attr("name", "priceferday" + "[" + idInventory + "]");
                priceFerDay.attr("size", "3px");
                priceFerDay.attr("required", "true");
                cell.html(priceFerDay);
                var cell = $(row.insertCell(-1));
                var milesFLT = $("<input />");
                milesFLT.attr("type", "text");
                milesFLT.attr("name", "days_miles" + "[" + idInventory + "]");
                milesFLT.attr("size", "3px");
                milesFLT.attr("required", "true");
                cell.html(milesFLT);
                var cell = $(row.insertCell(-1));
                var deliveryFee = $("<select />");
                deliveryFee.attr("required", "true");
                var selectList = [
                    {val: 1, text: 'Yes'},
                    {val: 0, text: 'No'}
                ];
                $(selectList).each(function () {
                    deliveryFee.append($("<option>").attr('value', this.val).text(this.text));
                });
                deliveryFee.attr("name", "delivery_fee" + "[" + idInventory + "]");
                deliveryFee.attr("class", "form-control");
                cell.html(deliveryFee);
                var cell = $(row.insertCell(-1));
                var comment = $("<input />");
                comment.attr("type", "text");
                comment.attr("name", "comment" + "[" + idInventory + "]");
                comment.attr("size", "20px");
                comment.attr("required", "true");
                cell.html(comment);
            }else {
            }
        }
    };
    function remove(button) {
        var row = $(button).closest("TR");
        var name = row.find('td:eq(1) input').val();
        var getVar = row.find('td:eq(1) input').attr('name');
        var getvalue = getVar.substr(6, 1);
        if (confirm("Do you want to delete: " + name)) {
            var table = $("#myTable")[0];
            table.deleteRow(row[0].rowIndex);
            $("#inventoryName").append('<option value="' + getvalue + '">' + name + '</option>');
            var order_id_detail = $(button).attr('data-orderid');
            var inventory_id_detail = $(button).attr('data-inventoryid');
            $.ajax({
                url: '<?php echo base_url('/cms/InventoryRentalController/delete') ?>',
                type: 'POST',
                data: {order_id_detail: order_id_detail, inventory_id_detail: inventory_id_detail},
                success: function () {
                }
            });
        }
    }
</script>