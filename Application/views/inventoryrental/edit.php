<div class="content-wrapper">
    <section class="content container-fluid">
        <div class="box">
            <form action="<?php echo base_url('inventoryrental/saveEdit'); ?>" method="post" class="form-horizontal">
                <h1 style="text-align: center">KC1 Project Job/ Inventory Rental</h1>
                <?php if (validation_errors()) { ?>
                    <div class="alert alert-error">
                        <?php echo validation_errors(); ?>
                    </div>
                <?php } ?>
                <div class="container">
                    <div class="form-group col-lg-6">
                        <div class="form-group">
                            <label class="control col-md-4">Inventory Rental ID: </label>
                            <div style="color:red;font-weight: bold"><?php echo $order_prefix . "-" . $inventory_order->order_id; ?></div>
                            <input type="hidden" name="order_id" value="<?php echo $inventory_order->order_id; ?>"/>
                        </div>
                        <div class="form-group">
                            <label class="control col-md-2">Company: </label>
                            <div class="col-md-9">
                                <input name="company" placeholder="" class="form-control" type="text"
                                       value="<?php echo $inventory_order->company ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control col-md-4">Company Representative: </label>
                            <div class="col-md-7">
                                <input name="representative" placeholder="" class="form-control" type="text"
                                       value="<?php echo $inventory_order->representative ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control col-md-2">Location: </label>
                            <div class="col-md-9">
                                <input name="location" placeholder="" class="form-control" type="text"
                                       value="<?php echo $inventory_order->location ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control col-md-2">Country: </label>
                            <div class="col-md-3">
                                <input name="country" placeholder="" class="form-control" type="text"
                                       value="<?php echo $inventory_order->country ?>">
                            </div>
                            <label class="control col-md-2">State: </label>
                            <div class="col-md-4">
                                <input name="state" placeholder="" class="form-control" type="text"
                                       value="<?php echo $inventory_order->state ?>">
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <div class="form-group">
                            <label class="control col-md-5">Order Date: </label>
                            <div class="col-md-5">
                                <input class="form-control" name="order_date"
                                       value="<?php echo $inventory_order->order_date ?>"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control col-md-3">Sales Person: </label>
                            <div class="col-md-9">
                                <input name="sales_person" placeholder="" class="form-control" type="text"
                                       value="<?php echo $inventory_order->sales_person ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control col-md-3">Delivered By: </label>
                            <div class="col-md-9">
                                <input name="delivered_by" placeholder="" class="form-control" type="text"
                                       value="<?php echo $inventory_order->delivered_by ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control col-md-3">AFE #: </label>
                            <div class="col-md-3">
                                <input name="afe" placeholder="" class="form-control" type="text"
                                       value="<?php echo $inventory_order->afe ?>">
                            </div>
                            <label class="control col-md-2">Truck #: </label>
                            <div class="col-md-4">
                                <input name="truck" placeholder="" class="form-control" type="text"
                                       value="<?php echo $inventory_order->truck ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control col-md-3">Mileages: </label>
                            <div class="col-md-3">
                                <input name="mileage" placeholder="" class="form-control" type="text"
                                       value="<?php echo $inventory_order->mileage ?>">
                            </div>
                            <label class="control col-md-2">Trailer: </label>
                            <div class="col-md-4">
                                <input name="trailer" placeholder="" class="form-control" type="text"
                                       value="<?php echo $inventory_order->trailer ?>">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <?php
                        $orderType = $inventory_order->order_type;
                        ?>
                        <?php
                        $type = array(
                            'Pump Down' => 'Pump Down',
                            'Frac Prep' => 'Frac Prep',
                            'Frac' => 'Frac',
                            'Drill Out' => 'Drill Out',
                            'Install Tubing' => 'Install Tubing',
                            'Coil Tubing' => 'Coil Tubing',
                            'Flow Back Completions' => 'Flow Back Completions',
                            'Flow Back Production' => 'Flow Back Production',
                            'Hydrostatic Pressure Test' => 'Hydrostatic Pressure Test',
                            'Misc' => 'Misc',
                            'Running Production' => 'Running Production',
                            'Backhoe/Dump Truck' => 'Backhoe/Dump Truck'
                        )
                        ?>
                        <?php
                        foreach ($type as $k => $v) { ?>
                            <?php if ($orderType == $k) { ?>
                                <div class="col-md-2">
                                    <label class="radio-inline">
                                        <input type="radio" name="order_type" value="<?php echo $k ?>"
                                               checked="checked"><?php echo $v; ?>
                                    </label>
                                </div>
                            <?php } else { ?>
                                <div class="col-md-2">
                                    <label class="radio-inline">
                                        <input type="radio" name="order_type"
                                               value="<?php echo $k ?>"><?php echo $v; ?>
                                    </label>
                                </div>
                            <?php } ?>
                        <?php } ?>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-7">
                            <div class="control-label col-md-1">
                                <label style="color: red">Status:</label>
                            </div>
                            <?php
                            $oderStatus = $inventory_order->order_status;
                            ?>
                            <?php
                            $status = array(
                                'Open' => 'Open',
                                'Closed' => 'Closed',
                                'Approved' => 'Approved',
                                'Invoiced' => 'Invoiced',
                                'Assigned' => 'Assigned'
                            ) ?>
                            <?php foreach ($status as $k => $v) { ?>
                                <?php if ($oderStatus == $k) { ?>
                                    <div class="col-md-2">
                                        <label class="radio-inline">
                                            <input type="radio" name="order_status" value="<?php echo $k ?>"
                                                   checked="checked"> <?php echo $v; ?>
                                        </label>
                                    </div>
                                <?php } else { ?>
                                    <div class="col-md-2">
                                        <label class="radio-inline">
                                            <input type="radio" name="order_status"
                                                   value="<?php echo $k ?>"> <?php echo $v; ?>
                                        </label>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-2">
                            <select class="form-control" id="inventoryName" name="inventoryName">
                                <option value="0">Select Inventory</option>
                                <?php
                                $inventoryArr = array();
                                $detailArr = array();
                                foreach ($inventory_name as $inventoryName) {
                                    $inventoryArr[] = $inventoryName;
                                }
                                foreach ($inventory_detail as $inventoryDetail) {
                                    $detailArr[] = $inventoryDetail;
                                }
                                ?>
                                <?php for ($i = 0; $i < count($inventoryArr); $i++) {
                                    $flash = false;
                                    ?>
                                    <?php echo $inventoryArr[$i]->inventory_name; ?>
                                    <?php

                                    for ($y = 0; $y < count($detailArr); $y++) {
                                        if ($inventoryArr [$i]->inventory_name == $detailArr[$y]->inventory_name) {
                                            $flash = true;
                                        }
                                        ?>
                                    <?php } ?>
                                    <?php if ($flash == true) { ?>
                                        <option style="display:none;"
                                                value="<?php echo $inventoryArr[$i]->inventory_id; ?>" <?php echo
                                        set_select('inventoryCategory', $inventoryArr[$i]->inventory_name); ?> ><?php echo $inventoryArr[$i]->inventory_name; ?></option>
                                    <?php } else {
                                        ?>
                                        <option value="<?php echo $inventoryArr[$i]->inventory_id; ?>" <?php echo
                                        set_select('inventoryCategory', $inventoryArr[$i]->inventory_name); ?> ><?php echo $inventoryArr[$i]->inventory_name; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-lg-2">
                            <th colspan="12">
                                <input type="button" name="add_inventory" class="btn btn-info" onclick="Add()"
                                       value="Add Inventory">
                            </th>
                        </div>
                    </div>
                    <div>
                        <div id="mainData">
                        </div>
                        <div class="form-group modal-footer">
                            <input type="button" class="btn btn-danger" value="Print">
                            <button type="button" class="btn btn-primary">Reset</button>
                            <input type="submit" name="btnUpdate" class="btn btn-info" value="Update">
                            <button type="button" class="btn btn-light">Cancel</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>
</div>

<script>

    loadData();

    function Add() {
        AddRow();
    }

    function AddRow() {
        var tBody = $("#myTable > TBODY")[0];
        var length = $('#inventoryName > option').length;
        var varSelect = $('#inventoryName').val();
        if (varSelect == 0) {
    } else {
            if (length > 0) {
                row = tBody.insertRow(-1);

                var cell = $(row.insertCell(-1));
                var btnRemove = $("<button />");
                btnRemove.attr("type", "button");
                btnRemove.append("<i class='fa fa-trash' aria-hidden='true'></i>");
                btnRemove.attr("onclick", "remove(this);");
                btnRemove.attr("size", "6px");
                var idHidden = $("<input />");
                var idInven = $('#inventoryName').val();
                idHidden.attr("value", idInven);
                idHidden.attr("name", "inventory_id[" + idInven + "]");
                idHidden.attr("type", "hidden");
                cell.append(btnRemove, idHidden);

                var cell = $(row.insertCell(-1));
                var inventoryName = $("<input />");
                var idInventory = $('#inventoryName').val();
                var nameInventory = $('#inventoryName :selected').text();
                inventoryName.attr("type", "text");
                inventoryName.prop('required',true);
                inventoryName.attr("id", "inventory_detail_id");
                inventoryName.attr("name", "inventory_name" + "[" + idInventory + "]");
                inventoryName.attr("value", nameInventory);
                inventoryName.attr("size", "5px");
                $("select[name=inventoryName] option:selected").remove();
                cell.append(inventoryName);

                var cell = $(row.insertCell(-1));
                var quantity = $("<input />");
                quantity.attr({
                    type: 'text',
                    required: true,
                    name:   'quantity' + '[' + idInventory + ']',
                    size:   '5px'
                });
                cell.html(quantity);

                var cell = $(row.insertCell(-1));
                var unitnumber = $("<input />");
                unitnumber.attr("type", "text");
                unitnumber.attr("value", "");
                unitnumber.attr("name", "unitnumber" + "[" + idInventory + "]");
                unitnumber.attr("size", "5px");
                cell.html(unitnumber);

                var cell = $(row.insertCell(-1));
                var start = $("<input />");
                start.attr("type", "date");
                start.attr("name", "start" + "[" + idInventory + "]");
                start.attr("size", "5px");
                cell.html(start);

                var cell = $(row.insertCell(-1));
                var finish = $("<input />");
                finish.attr("type", "date");
                finish.attr("name", "finish" + "[" + idInventory + "]");
                finish.attr("size", "5px");
                cell.html(finish);

                var cell = $(row.insertCell(-1));
                var delpu = $("<input />");
                delpu.attr("type", "checkbox");
                delpu.attr("name", "del_pu" + "[" + idInventory + "]");
                delpu.attr("size", "5px");
                cell.html(delpu);

                var cell = $(row.insertCell(-1));
                var ofdays = $("<input />");
                ofdays.attr("type", "text");
                ofdays.attr("name", "ofdays" + "[" + idInventory + "]");
                ofdays.attr("size", "3px");
                cell.html(ofdays);

                var cell = $(row.insertCell(-1));
                var pricePerday = $("<input />");
                pricePerday.attr("type", "text");
                pricePerday.attr("name", "priceFerDay" + "[" + idInventory + "]");
                pricePerday.attr("size", "3px");
                cell.html(pricePerday);

                var cell = $(row.insertCell(-1));
                var milesFLT = $("<input />");
                milesFLT.attr("type", "text");
                milesFLT.attr("name", "days_miles" + "[" + idInventory + "]");
                milesFLT.attr("size", "3px");
                cell.html(milesFLT);

                var cell = $(row.insertCell(-1));
                var deliveryFee = $("<select />");
                var selectList = [
                    {val: 1, text: 'Yes'},
                    {val: 0, text: 'No'}
                ];
                $(selectList).each(function () {
                    deliveryFee.append($("<option>").attr('value', this.val).text(this.text));
                });
                deliveryFee.attr("name", "delivery_fee" + "[" + idInventory + "]");
                deliveryFee.attr("class", "form-control");
                cell.html(deliveryFee);

                var cell = $(row.insertCell(-1));
                var comment = $("<input />");
                comment.attr("type", "text");
                comment.attr("name", "comment" + "[" + idInventory + "]");
                comment.attr("size", "20px");
                cell.html(comment);
            }else {
            }
        }
    };

    function loadData() {
        var id = <?php echo $inventory_order->order_id ?>;
        $.ajax({
            url: '<?php echo base_url('inventoryrental/editretail/') ?>' + id,
            success: function (data) {
                $('#mainData').html(data);
            }
        });
    }

    function remove(button) {
        var row = $(button).closest("TR");
        var name = row.find('td:eq(1) input').val();
        var getVar = row.find('td:eq(1) input').attr('name');
        var getvalue = getVar.substr(13, 1);

        if (confirm("Do you want to delete: " + name)) {
            var table = $("#myTable")[0];
            table.deleteRow(row[0].rowIndex);
            $("#inventoryName").append('<option value="' + getvalue + '">' + name + '</option>');

            var order_id_detail = $(button).attr('data-orderid');
            var inventory_id_detail = $(button).attr('data-inventoryid');

            $.ajax({
                url: '<?php echo base_url('/cms/InventoryRentalController/delete') ?>',
                type: 'POST',
                data: {order_id_detail: order_id_detail, inventory_id_detail: inventory_id_detail},
                success: function () {
                }
            });
        }

    }
</script>