<div class="content-wrapper">
    <section class="content container-fluid">
        <section class="content-header">
            <h3>Edit Category</h3>
            <a href="<?php echo base_url('category'); ?>" class="btn btn-default">Back</a>
        </section>

        <?php if (validation_errors()) { ?>
            <div class="alert alert-error">
                <?php echo validation_errors(); ?>
            </div>
        <?php } ?>

        <div class="box-body">
            <form action="<?php echo base_url('category/saveEdit') ?>" method="post" class="form-horizontal">
                <input type="hidden" name="txt_hidden" value="<?php echo $category->inventory_category_id; ?>">
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Category Name: </label>
                        <div class="col-md-5">
                            <input name="categoryName" value="<?php echo $category->inventory_category_name; ?>"
                                   class="form-control" type="text">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="modal-footer col-md-8">
                        <input type="submit" name="btnUpdate" class="btn btn-primary" value="Update">
                        <a href="<?php echo base_url('category'); ?>" class="btn btn-default">Cancel</a>
                    </div>
                </div>
            </form>
        </div>
    </section>
</div>

