<div class="content-wrapper">
    <section class="content container-fluid">
        <section class="content-header">
            <h3>Create Category</h3>
            <a href="<?php echo base_url('category'); ?>" class="btn btn-default">Back</a>
        </section>

        <?php if (validation_errors()) { ?>
            <div class="alert alert-error">
                <?php echo validation_errors(); ?>
            </div>
        <?php } ?>

        <div class="box-body">
            <form action="<?php echo base_url('category/saveNew') ?>" method="post" class="form-horizontal">
                <input type="hidden" value="" name="id"/>
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Category Name: </label>
                        <div class="col-md-5">
                            <input name="categoryName" placeholder="" class="form-control" type="text">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="modal-footer col-md-8">
                        <input type="submit" name="btnSave" class="btn btn-primary" value="Save">
                        <a href="<?php echo base_url('category'); ?>" class="btn btn-default">Cancel</a>
                    </div>
                </div>
            </form>
        </div>
    </section>
</div>

