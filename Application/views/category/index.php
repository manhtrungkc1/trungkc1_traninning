<div class="content-wrapper">
    <section class="content-header">
        <?php if ($this->session->flashdata('item')) { ?>
            <div class="alert alert-success">
                <?php echo $this->session->flashdata('item'); ?>
            </div>
        <?php } ?>
    </section>
    <section class="content container-fluid">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">List Category</h3>
                <br/>
                <a href="<?php echo base_url('category/add'); ?>" class="btn btn-primary"><i
                            class="glyphicon glyphicon-plus"></i> Add Category</a>
            </div>
            <div class="box-body">
                <table class="table table-striped table-hover table-bordered">
                    <thead>
                    <tr>
                        <th>Category ID</th>
                        <th>Category Name</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if ($categorys) {
                        foreach ($categorys as $category) {
                            ?>
                            <tr>
                                <td><?php echo $category->inventory_category_id; ?></td>
                                <td><?php echo $category->inventory_category_name; ?></td>
                                <td>
                                    <a href="<?php echo base_url('category/edit/' . $category->inventory_category_id); ?>"
                                       class="btn btn-info">Edit</a>
                                    <a href="<?php echo base_url('category/delete/' . $category->inventory_category_id); ?>"
                                       class="btn btn-danger"
                                       onclick="return confirm('Do you want to delete this category?');">Delete</a>
                                </td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>
<?php
