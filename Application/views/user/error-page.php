<div class="content-wrapper">
    <section class="content container-fluid">
        <section class="content-header">
            <h3>Error Page</h3>
            <a href="<?php echo base_url('user'); ?>" class="btn btn-default">Back</a>
        </section>

        <div class="box-body">
            <h2>This is Internal Error!</h2>
        </div>
    </section>
</div>