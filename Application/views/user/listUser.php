<div class="content-wrapper">
    <section class="content container-fluid">
        <section class="content-header">
            <h3>Data Table</h3>
        </section>
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">List User</h3>
                <br/>
                <button type="button" class="btn btn-success openBtn" id="add" data-toggle="modal"
                        data-target="#insert_data_Modal"><i class="glyphicon glyphicon-plus"></i> Add User
                </button>
            </div>
            <div class="box-body">
                <table id="table" class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Username</th>
                        <th>Password</th>
                        <th>Email</th>
                        <th>Create Date</th>
                        <th>Group Id</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">

    var table;

    $(document).ready(function () {

        table = $('#table').DataTable({

            "processing": true,
            "serverSide": true,
            "order": [],

            "ajax": {
                "url": "<?php echo site_url('user/ajax_list')?>",
                "type": "POST"
            },

            "columnDefs": [
                {
                    "targets": [-1],
                    "orderable": false,
                },
            ],
        });

        $('.openBtn').on('click', function () {
            $('#insert-modal-body').load('<?php echo base_url('user/addForm'); ?>', function () {
                $('#insert_data_Modal').modal({show: true});
            });
        });

        $('.openBtn1').on('click', function () {
            var editUrl = '<?php echo base_url('user/editForm'); ?>';
            console.log(editUrl);
            $('#edit-modal-body').load('<?php echo base_url('user/editForm'); ?>', function () {

            });
            $('#edit_data_Modal').modal({show: true});
        });

    });

    function reload_table() {
        table.ajax.reload(null, false);
    }

    function save_user() {
        if (validateAddUser() == false) {
            return false;
        }
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('user/ajax_add')?>",
            data: $("#insert_modal_form	").serialize(),
            dataType: "JSON",
            success: function (data) {
                $('#insert_data_Modal').modal('hide');
                reload_table();
            },
        });
        //return false;
    }

    function validateAddUser() {
        var status = true;
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('user/validate_add')?>",
            //dataType: "JSON",
            async: false,
            data: $("#insert_modal_form	").serialize(),
            success: function (data) {
                var data = jQuery.parseJSON(data);
                status = data.status;
                if (data.status) {
                    $('#insert_data_Modal').modal('hide');
                    reload_table();
                } else {
                    for (var i = 0; i < data.inputerror.length; i++) {
                        $('[name="' + data.inputerror[i] + '"]').parent().parent().addClass('has-error');
                        $('[name="' + data.inputerror[i] + '"]').next().text(data.error_string[i]);
                    }
                }
            },
        });
        return status;
    }

    function validateEditUser() {
        var status = true;
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('user/validate_edit')?>",
            //dataType: "JSON",
            async: false,
            data: $("#edit_modal_form").serialize(),
            success: function (data) {
                var data = jQuery.parseJSON(data);
                status = data.status;
                if (data.status) {
                    $('#edit_data_Modal').modal('hide');
                    reload_table();
                } else {
                    for (var i = 0; i < data.inputerror.length; i++) {
                        $('[name="' + data.inputerror[i] + '"]').parent().parent().addClass('has-error');
                        $('[name="' + data.inputerror[i] + '"]').next().text(data.error_string[i]);
                    }
                }
            },
        });
        return status;
    }

    function editForm(id) {
        //Ajax Load data from ajax
        $.ajax({
            url: "<?php echo site_url('user/ajax_edit')?>/" + id,
            type: "GET",
            success: function (data) {
                $('#edit-modal-body').html(data);
                $('#edit_data_Modal').modal({show: true});
            },
        });
    }

    function edit_user(id) {
        if (validateEditUser() == false) {
            return false;
        }
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('user/ajax_update')?>/" + id,
            //dataType: "JSON",
            data: $("#edit_modal_form").serialize(),
            success: function (data) {
                $('#edit_data_Modal').modal('hide');
                reload_table();
            },
        });
    }

    function delete_user(id) {
        if (confirm('Are you sure delete this user?')) {
            $.ajax({
                url: "<?php echo site_url('user/ajax_delete')?>/" + id,
                type: "POST",
                dataType: "JSON",
                success: function (data) {
                    $('#modal_form').modal('hide');
                    reload_table();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert('Error deleting data');
                }
            });

        }
    }
</script>

<div id="insert_data_Modal" class="modal fade">
    <div class="modal-dialog" id="myModal">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"></button>
                <h4 class="modal-title"> Create User</h4>
            </div>
            <div class="modal-body" id="insert-modal-body">
            </div>
        </div>
    </div>
</div>

<div id="edit_data_Modal" class="modal fade">
    <div class="modal-dialog" id="myModal">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"></button>
                <h4 class="modal-title"> Edit User</h4>
            </div>
            <div class="modal-body" id="edit-modal-body">
            </div>
        </div>
    </div>
</div>

</body>
