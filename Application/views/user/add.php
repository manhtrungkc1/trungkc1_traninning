<div class="content-wrapper">
    <section class="content container-fluid">
        <section class="content-header">
            <h3>Add User</h3>
            <a href="<?php echo base_url('user'); ?>" class="btn btn-default">Back</a>
        </section>

        <?php if (validation_errors()) { ?>
            <div class="alert alert-error">
                <?php echo validation_errors(); ?>
            </div>
        <?php } ?>

        <div class="box-body">
            <form action="<?php echo base_url('user/saveNew') ?>" method="post" class="form-horizontal">
                <input type="hidden" value="" name="id"/>
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Username: </label>
                        <div class="col-md-5">
                            <input name="userName" placeholder="Username" value="<?php echo set_value('userName'); ?>"
                                   class="form-control" type="text">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Password: </label>
                        <div class="col-md-5">
                            <input name="passw" placeholder="******" value="<?php echo set_value('passw'); ?>"
                                   class="form-control" type="password">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Confirm Password: </label>
                        <div class="col-md-5">
                            <input name="cfmpass" placeholder="******" value="<?php echo set_value('cfmpass'); ?>"
                                   class="form-control" type="password">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Email</label>
                        <div class="col-md-5">
                            <input name="email" placeholder="@gmail.com" value="<?php echo set_value('email'); ?>"
                                   class="form-control">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">User Group:</label>
                        <div class="form-check-inline col-md-5">
                            <select class="form-control" name="groupId">
                                <option value=''>--Select of Group</option>
                                <?php foreach ($user_group as $group) { ?>
                                    <option value="<?php echo $group->group_id ?>" <?php echo
                                    set_select('groupId', $group->group_id); ?> > <?php echo $group->group_name; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Phone: </label>
                        <div class="col-md-5">
                            <input name="phone" placeholder="Phone" value="<?php echo set_value('phone'); ?>"
                                   class="form-control" type="text">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Create Date: </label>
                        <div class="col-md-5">
                            <input type="datetime-local" name="createDate"
                                   value="<?php echo set_value('createDate'); ?>"/>
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="modal-footer col-md-8">
                        <input type="submit" name="btnSave" class="btn btn-primary" value="Save">
                        <a href="<?php echo base_url('user'); ?>" class="btn btn-default">Cancel</a>
                    </div>
                </div>
            </form>
        </div>
    </section>
</div>

