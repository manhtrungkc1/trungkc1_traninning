<div class="content-wrapper">
    <section class="content-header">
        <?php if ($this->session->flashdata('item')) { ?>
            <div class="alert alert-success">
                <?php echo $this->session->flashdata('item'); ?>
            </div>
        <?php } ?>
    </section>
    <section class="content container-fluid">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">List User</h3>
                <br/>
                <a href="<?php echo base_url('user/add'); ?>" class="btn btn-primary"><i
                            class="glyphicon glyphicon-plus"></i> Add User</a>
            </div>
            <div class="box-body">
                <table class="table table-striped table-hover table-bordered">
                    <thead>
                    <tr>
                        <th>User ID</th>
                        <th>Username</th>
                        <th>Password</th>
                        <th>Email</th>
                        <th>Group Id</th>
                        <th>Phone</th>
                        <th>Last Login</th>
                        <th>Create_at</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if ($users) {
                        foreach ($users as $user) {
                            ?>
                            <tr>
                                <td><?php echo $user['user_id']; ?></td>
                                <td><?php echo $user['username']; ?></td>
                                <td><?php echo $user['password']; ?></td>
                                <td><?php echo $user['email']; ?></td>
                                <td><?php echo $user['group_name']; ?></td>
                                <td><?php echo $user['phone']; ?></td>
                                <td><?php echo $user['last_login']; ?></td>
                                <td><?php echo $user['created_at']; ?></td>
                                <td>
                                    <a href="<?php echo base_url('user/edit/' . $user['user_id']); ?>"
                                       class="btn btn-info">Edit</a>
                                    <a href="<?php echo base_url('user/delete/' . $user['user_id']); ?>"
                                       class="btn btn-danger"
                                       onclick="return confirm('Do you want to delete this user?');">Delete</a>
                                </td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                    </tbody>
                </table>
                <div class="dataTables_paginate paging_simple_numbers">
                    <ul class="pagination">
                        <?php echo $pagination ;?>
                    </ul>
                </div>

            </div>
        </div>
    </section>
</div>
