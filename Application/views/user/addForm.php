<form action="#" id="insert_modal_form" class="form-horizontal">
    <input type="hidden" value="" name="id"/>
    <div class="form-body">
        <div class="form-group">
            <label class="control-label col-md-3">First Name</label>
            <div class="col-md-9">
                <input id="firstName" name="firstName" placeholder="First Name" class="form-control" type="text">
                <span class="help-block"></span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3">Last Name</label>
            <div class="col-md-9">
                <input name="lastName" placeholder="Last Name" class="form-control" type="text">
                <span class="help-block"></span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3">Username</label>
            <div class="col-md-9">
                <input name="userName" placeholder="Username" class="form-control" type="text">
                <span class="help-block"></span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3">Password</label>
            <div class="col-md-9">
                <input name="passw" placeholder="******" class="form-control" type="password">
                <span class="help-block"></span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3">Confirm Password</label>
            <div class="col-md-9">
                <input name="cfmpass" placeholder="******" class="form-control" type="password">
                <span class="help-block"></span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3">Email</label>
            <div class="col-md-9">
                <input name="email" placeholder="@gmail.com" class="form-control" type="email">
                <span class="help-block"></span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3">Create Date</label>
            <div class="col-md-9">
                <input type="date" name="createDate"/>
                <span class="help-block"></span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3">User Group:</label>
            <div class="col-md-5">
                <select class="form-control" id="groudId" name="groupId">
                    <option value="1">Admin</option>
                    <option value="2">Semi-Admin</option>
                    <option value="3">Member</option>
                </select>
            </div>
        </div>
    </div>
    </div>
</form>
<div class="modal-footer">
    <button type="button" id="btn_save" onclick="save_user()" class="btn btn-primary">Save</button>
    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
</div>

