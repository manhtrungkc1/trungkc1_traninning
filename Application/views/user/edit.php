<div class="content-wrapper">
    <section class="content container-fluid">
        <section class="content-header">
            <h3>Edit User</h3>
            <a href="<?php echo base_url('user'); ?>" class="btn btn-default">Back</a>
        </section>

        <?php if (validation_errors()) { ?>
            <div class="alert alert-error">
                <?php echo validation_errors(); ?>
            </div>
        <?php } ?>

        <div class="box-body">
            <form action="<?php echo base_url('user/saveEdit') ?>" method="post" class="form-horizontal">
                <input type="hidden" name="txt_hidden" value="<?php echo $user->user_id; ?>">
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Username: </label>
                        <div class="col-md-5">
                            <input name="userName" value="<?php echo $user->username; ?>" class="form-control"
                                   type="text">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Password: </label>
                        <div class="col-md-5">
                            <input name="passw" value="<?php echo $user->password; ?>" class="form-control"
                                   type="password">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Confirm Password: </label>
                        <div class="col-md-5">
                            <input name="cfmpass" value="<?php echo $user->password; ?>" class="form-control"
                                   type="password">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Email: </label>
                        <div class="col-md-5">
                            <input name="email" value="<?php echo $user->email; ?>" class="form-control" type="email">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Group Id: </label>
                        <div class="form-check-inline col-md-5">
                            <select class="form-control" name="groupId">
                                <?php
                                $group_user = $user->group_id;
                                ?>
                                <?php foreach ($user_group as $group) { ?>
                                    <?php if ($group_user == $group->group_id) { ?>
                                        <option type="radio" name="groupId" value="<?php echo $group->group_id ?>"
                                                selected> <?php echo $group->group_name; ?></option>
                                    <?php } else { ?>
                                        <option type="radio" name="groupId"
                                                value="<?php echo $group->group_id ?>"> <?php echo $group->group_name; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Phone: </label>
                        <div class="col-md-5">
                            <input name="phone" value="<?php echo $user->phone; ?>" class="form-control" type="text">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Create At: </label>
                        <div class="col-md-5">
                            <input type="text" name="date" value="<?php echo $user->created_at; ?>" readonly="true"/>
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="modal-footer col-md-8">
                        <input type="submit" name="btnUpdate" class="btn btn-primary" value="Update">
                        <a href="<?php echo base_url('user'); ?>" class="btn btn-default">Cancel</a>
                    </div>
                </div>
            </form>
        </div>
    </section>
</div>

