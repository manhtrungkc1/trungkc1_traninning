<form action="#" id="edit_modal_form" class="form-horizontal">
    <input type="hidden" value="<?php echo $id; ?>" name="id"/>
    <div class="form-body">
        <div class="form-group">
            <label class="control-label col-md-3">First Name</label>
            <div class="col-md-9">
                <input id="firstName" name="firstName" class="form-control" type="text"
                       value="<?php echo $firstName ?>"/>
                <span class="help-block"></span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3">Last Name</label>
            <div class="col-md-9">
                <input name="lastName" class="form-control" type="text" value="<?php echo $lastName ?>"/>
                <span class="help-block"></span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3">Username</label>
            <div class="col-md-9">
                <input name="userName" class="form-control" type="text" value="<?php echo $userName ?>"/>
                <span class="help-block"></span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3">Password</label>
            <div class="col-md-9">
                <input id="passw" name="passw" class="form-control" type="password" value="<?php echo $passw ?>"/>
                <span class="help-block"></span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3">Confirm Password</label>
            <div class="col-md-9">
                <input id="cfmpass" name="cfmpass" class="form-control" type="password" value="<?php echo $cfmpass ?>"/>
                <span class="help-block"></span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3">Email</label>
            <div class="col-md-9">
                <input id="email" name="email" class="form-control" type="text" value="<?php echo $email ?>"/>
                <span class="help-block"></span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3">Create Date</label>
            <div class="col-md-9">
                <input id="createDate" name="createDate" class="form-control" type="date"
                       value="<?php echo $createDate ?>"/>
                <span class="help-block"></span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3">User Group:</label>
            <div class="col-md-4">
                <select class="form-control" id="status" name="groupId">
                    <option value="1">Admin</option>
                    <option value="2">Semi-Admin</option>
                    <option value="3">Member</option>
                </select>
            </div>
        </div>
    </div>
    </div>
</form>
<div class="modal-footer">
    <button type="button" id="btn_edit" onclick="edit_user()" class="btn btn-primary">Edit</button>
    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
</div>

