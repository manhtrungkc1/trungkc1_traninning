<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Model
{

    public function getCategory()
    {
        $this->db->order_by('inventory_category_id');
        $query = $this->db->get('inventory_category');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function getCategoryById($id)
    {
        $this->db->where('inventory_category_id', $id);
        $query = $this->db->get('inventory_category');
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    function insertCategory($field)
    {
        $this->db->insert('inventory_category', $field);
    }

    public function updateCategory($field,$id)
    {
        $this->db->where('inventory_category_id', $id);
        $this->db->update('inventory_category', $field);
    }

    public function deleteCategory($id)
    {
        $this->db->where('inventory_category_id', $id);
        $this->db->delete('inventory_category');
    }
}