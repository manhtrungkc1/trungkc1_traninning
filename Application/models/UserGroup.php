<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class UserGroup extends CI_Model
{

    public function getUserGroup()
    {
        $this->db->order_by('group_id');
        $query = $this->db->get('group');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }


    public function getUserGroupById($id)
    {
        $this->db->where('group_id', $id);
        $query = $this->db->get('group');
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function insertGroup($field){
        $this->db->insert('group', $field);
    }

    public function updateGroup($field,$id){
        $this->db->where('group_id', $id);
        $this->db->update('group', $field);
    }

    public function deleteGroup($id)
    {
        $this->db->where('group_id', $id);
        $this->db->delete('group');
    }
}