<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Model
{

    public function listUser2($record, $start)
    {
        $sql = 'SELECT 
    u.user_id,
    u.username,
    u.password,
    u.email,
    ug.group_name,
    u.phone,
    u.last_login,
    u.created_at
FROM 
	user u
		INNER JOIN 
			`group` ug ON u.group_id = ug.group_id
			ORDER BY user_id
			';
        $this->db->limit($record,$start);
       /* $query = $this->db->get("user");*/
        /*$this->db->order_by('user_id');
      $query = $this->db->get('user');*/
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
       /*     return $query->result();*/
        } else {
            return false;
        }
    }

    public function submit()
    {
        $result = $this->m->submit();
        if ($result) {
            $this->session->set_flashdata('success_msg', 'Add user successfully');
        } else {
            $this->session->set_flashdata('error_msg', 'Faill to add user');
        }
    }

    public function getUserById($id)
    {
        $this->db->where('user_id', $id);
        $query = $this->db->get('user');
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function count_all($table){
        $count = $this->db->count_all($table);
        return $count;
    }

    function insert($field)
    {
        $this->db->insert('user', $field);
    }

    function updateUser($field,$id){ //update mang filed where = $id
        $this->db->where('user_id', $id);
        $this->db->update('user', $field);
    }

    public function deleteUser($id){
        $this->db->where('user_id', $id);
        $this->db->delete('user');
    }
}
