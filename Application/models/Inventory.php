<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Inventory extends CI_Model
{

    public function getInventory()
    {
        $sql = '
            SELECT 
                i.inventory_id,
                i.inventory_name,
                i.rent_type,
                ic.inventory_category_name
            FROM
                inventory i
                    INNER JOIN
                inventory_category ic ON i.inventory_category_id = ic.inventory_category_id
            ORDER BY inventory_id

        ';
        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            //return $query->result('Inventory');
            return $query->result();
        } else {
            return false;
        }
    }


    public function getInventoryById($id)
    {
        $this->db->where('inventory_id', $id);
        $query = $this->db->get('inventory');
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function insertInventory($field){
        $this->db->insert('inventory', $field);
        $this->session->set_flashdata('item', 'Added successfully');
    }

    public function updateInventory($field,$id)
    {
        $this->db->where('inventory_id', $id);
        $this->db->update('inventory', $field);
    }

    public function deleteInventory($id)
    {
        $this->db->where('inventory_id', $id);
        $this->db->delete('inventory');
    }
}