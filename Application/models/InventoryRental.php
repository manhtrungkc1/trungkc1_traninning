<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class InventoryRental extends CI_Model
{
    public function getIncrementOrderId()
    {
        $order_id = 0;
        $sql = 'SELECT MAX(`order_id`) as order_id FROM `inventory_rental_order`';
        $order_id = $this->db->query($sql)->row('order_id');
        $order_id = $order_id + 1;
        return $order_id;
    }

    public function getInventoryRental()
    {
        $this->db->order_by('order_id');
        $query = $this->db->get('inventory_rental_order');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function getInventoryOrderById($id)
    {
        $this->db->where('order_id', $id);
        $query = $this->db->get('inventory_rental_order');
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function insertRentalOrder($field){
        $this->db->insert('inventory_rental_order', $field);
    }

    public function updateRentalOrder($field,$id){
        $this->db->where('order_id', $id);
        $this->db->update('inventory_rental_order', $field);
    }
}