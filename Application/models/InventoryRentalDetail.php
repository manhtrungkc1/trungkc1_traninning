<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class InventoryRentalDetail extends CI_Model
{

    public function getInventoryDetail($id)
    {
        $sql = "SELECT 
	i.order_id,
	i.inventory_id,
    i.quantity,
    i.start,
    i.finish,
    i.delivery_fee,
    i.delpu,
    i.comment,
    i.days_miles,
    i.unitnumber,
    ic.inventory_name
    FROM 
    inventory_rental_order_detail i
    INNER JOIN 
    inventory ic ON i.inventory_id = ic.inventory_id
    WHERE order_id=" . $id;
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function insertInventoryRentalDetail($value){
        $this->db->insert('inventory_rental_order_detail', $value);
    }

    public function updateInventoryRentalDetail($order,$id_order){
        $this->db->where('order_id', $id_order);
        $this->db->update('inventory_rental_order_detail', $order);
    }

    public function deleteInventoryRentalDetail($order_id){
        $this->db->where('order_id', $order_id);
        $this->db->delete('inventory_rental_order_detail');
    }

}