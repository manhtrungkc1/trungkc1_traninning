-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 02, 2019 at 07:15 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 5.6.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kc1`
--

-- --------------------------------------------------------

--
-- Table structure for table `group`
--

CREATE TABLE `group` (
  `group_id` int(11) NOT NULL,
  `group_name` varchar(255) NOT NULL,
  `management_group` tinyint(4) NOT NULL,
  `delete_inventory` tinyint(4) NOT NULL,
  `edit_inventory` tinyint(4) NOT NULL,
  `deleted` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `group`
--

INSERT INTO `group` (`group_id`, `group_name`, `management_group`, `delete_inventory`, `edit_inventory`, `deleted`) VALUES
(1, 'Admin', 1, 1, 1, 0),
(2, 'Moderator', 0, 1, 1, 0),
(3, 'Member', 1, 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `inventory`
--

CREATE TABLE `inventory` (
  `inventory_id` int(11) NOT NULL,
  `inventory_name` varchar(255) NOT NULL,
  `rent_type` varchar(255) NOT NULL,
  `inventory_category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `inventory`
--

INSERT INTO `inventory` (`inventory_id`, `inventory_name`, `rent_type`, `inventory_category_id`) VALUES
(1, 'Truck', 'Day', 1),
(2, 'Lift', 'Day', 4),
(3, 'Fork', 'Day', 2),
(4, 'USB', 'Mile', 4),
(5, 'IT', 'Day', 3),
(6, 'BOD', 'Day', 2);

-- --------------------------------------------------------

--
-- Table structure for table `inventory_category`
--

CREATE TABLE `inventory_category` (
  `inventory_category_id` int(11) NOT NULL,
  `inventory_category_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `inventory_category`
--

INSERT INTO `inventory_category` (`inventory_category_id`, `inventory_category_name`) VALUES
(1, 'Apple'),
(2, 'Sony'),
(3, 'Xiaomi'),
(4, 'Samsung'),
(5, 'BlackBerry');

-- --------------------------------------------------------

--
-- Table structure for table `inventory_rental_order`
--

CREATE TABLE `inventory_rental_order` (
  `order_id` int(11) NOT NULL,
  `company` varchar(255) NOT NULL,
  `representative` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `order_type` varchar(255) NOT NULL,
  `order_status` varchar(15) NOT NULL,
  `sales_person` varchar(255) NOT NULL,
  `delivered_by` varchar(255) NOT NULL,
  `mileage` int(11) NOT NULL,
  `afe` varchar(255) NOT NULL,
  `truck` varchar(255) NOT NULL,
  `trailer` varchar(255) NOT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `order_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `inventory_rental_order`
--

INSERT INTO `inventory_rental_order` (`order_id`, `company`, `representative`, `location`, `country`, `state`, `order_type`, `order_status`, `sales_person`, `delivered_by`, `mileage`, `afe`, `truck`, `trailer`, `deleted`, `order_date`) VALUES
(1, '01', '01', '01', '01', '01', 'Running Production', 'Assigned', '01', '01', 1, '01', '01', '01', NULL, '2019-03-29 13:05:12'),
(2, '22', '22', '22', '22', '22', '', 'Assigned', '22', '22', 22, '22', '22', '22', NULL, '2019-03-29 15:05:05'),
(3, '123', '123', '123', '123', '123', 'Running Production', 'Invoiced', '123', '123', 123, '123', '123', '123', NULL, '2019-03-29 15:08:18'),
(4, '44', '44', '44', '44', '44', 'on', 'Assigned', '44', '44', 44, '44', '44', '44', NULL, '2019-03-29 15:09:20'),
(5, '55', '5555', '5555', 'v', 'v', 'on', 'Assigned', '5555', '5555', 55, '55', '55', '55', NULL, '2019-03-29 15:14:23');

-- --------------------------------------------------------

--
-- Table structure for table `inventory_rental_order_detail`
--

CREATE TABLE `inventory_rental_order_detail` (
  `order_id` int(11) NOT NULL,
  `inventory_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `start` date NOT NULL,
  `finish` date NOT NULL,
  `delpu` tinyint(4) NOT NULL,
  `delivery_fee` tinyint(4) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `days_miles` int(11) DEFAULT NULL,
  `unitnumber` int(11) NOT NULL,
  `of_days` int(11) NOT NULL,
  `priceferday` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `inventory_rental_order_detail`
--

INSERT INTO `inventory_rental_order_detail` (`order_id`, `inventory_id`, `quantity`, `start`, `finish`, `delpu`, `delivery_fee`, `comment`, `days_miles`, `unitnumber`, `of_days`, `priceferday`) VALUES
(1, 2, 1, '0000-00-00', '0000-00-00', 1, 1, '', 11, 1, 11, 0),
(1, 3, 1, '0000-00-00', '0000-00-00', 0, 0, '', 1, 1, 1, 0),
(1, 4, 1, '0000-00-00', '0000-00-00', 0, 0, '', 1, 1, 1, 0),
(2, 1, 0, '0000-00-00', '0000-00-00', 1, 1, '', 0, 0, 0, 0),
(3, 2, 0, '0000-00-00', '0000-00-00', 0, 1, '', 0, 0, 0, 0),
(4, 2, 0, '0000-00-00', '0000-00-00', 0, 1, '', 0, 0, 0, 0),
(5, 1, 1, '0000-00-00', '0000-00-00', 1, 1, '', 1, 1, 1, 0),
(5, 3, 1, '0000-00-00', '0000-00-00', 0, 0, '', 1, 1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `group_id` tinyint(4) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `last_login` datetime NOT NULL,
  `deleted` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `username`, `password`, `email`, `group_id`, `phone`, `last_login`, `deleted`, `created_at`) VALUES
(7, 'anderherrera', 'herrera', 'herreraander@gmail.com', 3, '0909996789', '0000-00-00 00:00:00', 0, '2019-02-19 10:06:00'),
(8, 'victorhugo', 'hugo27', 'victorhugo@gmail.com', 1, '00-888-777-77', '0000-00-00 00:00:00', 0, '2019-02-19 09:52:00'),
(9, 'matic31', 'maticmanutd', 'matic31@gmail.com', 3, '00-77-6789022', '0000-00-00 00:00:00', 0, '2019-02-20 16:02:00'),
(10, 'trungnguyen271', 'Trung2701', 'vamos.rooney271@gmail.com', 2, '0983127192', '0000-00-00 00:00:00', 0, '2019-02-20 14:02:00'),
(12, 'trung271', '123123', 'Tr123@gmail.com', 3, '0983127192', '0000-00-00 00:00:00', 0, '2019-02-21 02:02:00'),
(13, 'superRumble', 'Rumble123', 'rumble271@gmail.com', 1, '00-888-777-65', '0000-00-00 00:00:00', 0, '2019-02-21 02:02:00'),
(14, '123', '123', '123@gmail.com', 2, '00-888-777-65', '0000-00-00 00:00:00', 0, '2019-02-21 14:03:00'),
(18, '6868', '12', '2@gmail.com', 3, '0983127192', '0000-00-00 00:00:00', 0, '2019-02-21 02:01:00'),
(19, '6753737', '7378378', 'vamos.rooney271@gmail.com', 2, '0983127192', '0000-00-00 00:00:00', 0, '2019-02-21 13:01:00'),
(20, '55555', '55', 'vamos.rooney271@gmail.com', 3, 'qw', '0000-00-00 00:00:00', 0, '2019-02-21 14:06:00'),
(21, 'qdwd', '123123', 'vamos.rooney271@gmail.com', 2, '0983127192', '0000-00-00 00:00:00', 0, '2019-02-06 02:34:00'),
(22, '123', '123123', '123@gmail.com', 2, '123', '0000-00-00 00:00:00', 0, '2019-03-27 14:31:00'),
(23, '123', '123', '11@gmail.com', 2, '00-888-777-65', '0000-00-00 00:00:00', 0, '2019-03-27 12:31:00'),
(24, '222', '222', 'vamos.rooney271@gmail.com', 2, '123123123', '0000-00-00 00:00:00', 0, '2019-03-19 12:31:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `group`
--
ALTER TABLE `group`
  ADD PRIMARY KEY (`group_id`);

--
-- Indexes for table `inventory`
--
ALTER TABLE `inventory`
  ADD PRIMARY KEY (`inventory_id`);

--
-- Indexes for table `inventory_category`
--
ALTER TABLE `inventory_category`
  ADD PRIMARY KEY (`inventory_category_id`);

--
-- Indexes for table `inventory_rental_order`
--
ALTER TABLE `inventory_rental_order`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `inventory_rental_order_detail`
--
ALTER TABLE `inventory_rental_order_detail`
  ADD PRIMARY KEY (`order_id`,`inventory_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `group`
--
ALTER TABLE `group`
  MODIFY `group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `inventory`
--
ALTER TABLE `inventory`
  MODIFY `inventory_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `inventory_category`
--
ALTER TABLE `inventory_category`
  MODIFY `inventory_category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `inventory_rental_order`
--
ALTER TABLE `inventory_rental_order`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
