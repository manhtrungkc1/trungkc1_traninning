======================================================================================================
-- Update: 18/02/2019.
-- By: trung n.
-- Task 5: Database design.

======================================================================================================

CREATE TABLE IF NOT EXISTS `user`(
`user_id` int(11) NOT NULL AUTO_INCREMENT,
`username` varchar(255) NOT NULL,
`password` varchar(255) NOT NULL,
`email` varchar(255) NOT NULL,
`group_id` tinyint(4) NOT NULL,
`phone` varchar(15) NOT NULL,
`last_login` datetime NOT NULL,
`deleted` tinyint(4) NOT NULL,
`created_at` datetime NOT NULL,
PRIMARY KEY (`user_id`)
)  ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `group`(
`group_id` int(11) NOT NULL AUTO_INCREMENT,
`group_name` varchar(255) NOT NULL,
`management_group` tinyint(4) NOT NULL,
`delete_inventory` tinyint(4) NOT NULL,
`edit_inventory` tinyint(4) NOT NULL,
`deleted` tinyint(4) NOT NULL,
PRIMARY KEY (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `inventory` (
`inventory_id` int(11) NOT NULL AUTO_INCREMENT,
`inventory_name` varchar(255) NOT NULL,
`rent_type` varchar(255) NOT NULL,
`inventory_category_id` int(11) NOT NULL,
PRIMARY KEY (`inventory_id`)
) ENGINE=InnoDB DEFAULT CHARSET= utf8 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `inventory_category` (
`inventory_category_id` int(11) NOT NULL AUTO_INCREMENT,
`inventory_category_name` varchar(255) NOT NULL,
PRIMARY KEY (`inventory_category_id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `inventory_rental_order_detail` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`order_id` int(11) NOT NULL,
`invetory_id` int(11) NOT NULL,
`quantity` int(11) NOT NULL,
`start` datetime NOT NULL,
`finish` datetime NOT NULL,
`delivery_fee` tinyint(4) NOT NULL,
`delpu` tinyint(4) NOT NULL,
`comment` varchar(255) NOT NULL,
`days_miles` int(11),
PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `inventory_rental_order` (
`order_id` int(11) NOT NULL AUTO_INCREMENT,
`company` varchar(255) NOT NULL,
`representative` varchar(255) NOT NULL,
`location` varchar(255) NOT NULL,
`country` varchar(255) NOT NULL,
`state` varchar(255) NOT NULL,
`order_type` varchar(255) NOT NULL,
`order_status` varchar(15) NOT NULL,
`sales_person` varchar(255) NOT NULL,
`delivered_by` varchar(255) NOT NULL,
`mileage`int(11) NOT NULL,
`afe` varchar(255) NOT NULL,
`truck` varchar(255) NOT NULL,
`trailer` varchar(255) NOT NULL,
`deleted` tinyint(4),
`order_date` datetime,
PRIMARY KEY (`order_id`)
) ENGINE = InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;