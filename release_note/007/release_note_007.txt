======================================================================================================
-- Update: 20/02/2019.
-- By: trung n.
-- #4 - Task 3: Inventory and Inventory Category

======================================================================================================

A   application/controllers/cms/InventoryRentalController.php
A   application/models/InventoryRental.php
A   application/views/inventoryrental/add.php

U   application/config/config.php
U   application/config/routes.php
U   application/controllers/cms/InventoryController.php
U   application/controllers/cms/UserController.php
U   application/controllers/cms/UserGroupController.php
U   application/models/Inventory.php
U   application/models/User.php
U   application/models/UserGroup.php
U   application/views/inventory/add.php
u   application/views/inventory/edit.php
U   application/views/usergroup/edit.php
U   application/views/user/add.php
U   application/views/user/edit.php
